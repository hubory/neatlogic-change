English / [Chinese](README.md)

## about

Neatlogic-change is a change module applied to change scenarios, which includes functions such as change creation nodes, change processing nodes, and change management.

## Feature

### Change Node

In process management, configure processes that contain event nodes. Change processing and change creation nodes must be associated group by group.
![img.png](README_IMAGES/img.png)
- Support informs them of their functionality
- Supports time-sensitive policies that associate priorities
- Support for specifying agents, or configuring dispatcher rules, such as assign agents by group member workload.

### Initiate Changes、Change Handling

The change creation node supports gradually adding change steps and directly referencing change templates. The change processing node is processed sequentially by the change step handler.
![img.png](README_IMAGES/img1.png)
![img.png](README_IMAGES/img2.png)

### Change Management

Change management is a page for managing change templates, adding segmented SOP templates, and combining them into change templates.
![img.png](README_IMAGES/img3.png)
- Support adding duplicate SOP templates
- Support for dragging and sorting SOP templates
- When creating changes, the grouping and type of the template can serve as filtering criteria.
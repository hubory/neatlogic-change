中文 / [English](README.en.md)

## 关于

neatlogic-change是变更模块，应用于变更场景，包含变更创建节点、变更处理节点、变更管理等功能。

## 主要功能

### 变更节点

在流程管理中配置包含事件节点的流程，变更处理和变更创建节点必须成对关联。
![img.png](README_IMAGES/img.png)
- 支持通知功能
- 支持关联优先级的时效策略
- 支持指定处理人，或者配置分派器规则，如按分组成员工作量分配处理人

### 发起变更、变更处理

变更创建节点支持逐步添加变更步骤，也支持直接引用变更模板。变更处理节点由变更步骤处理人按顺序处理变更步骤。
![img.png](README_IMAGES/img1.png)
![img.png](README_IMAGES/img2.png)

### 变更管理

变更管理是管理变更模板的页面，添加细分的sop模板，将sop模板组合成变更模板。
![img.png](README_IMAGES/img3.png)
- 支持添加重复的sop模板
- 支持拖动sop模板排序
- 创建变更时，模板的分组和类型可作为过滤的条件。
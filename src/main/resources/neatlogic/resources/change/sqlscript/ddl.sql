-- ----------------------------
-- Table structure for change
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change` (
  `id` bigint NOT NULL COMMENT '唯一主键id',
  `plan_start_time` varchar(23) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '计划开始时间',
  `plan_end_time` varchar(23) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '计划结束时间',
  `start_time` timestamp(3) NULL DEFAULT NULL COMMENT '实际开始时间',
  `end_time` timestamp(3) NULL DEFAULT NULL COMMENT '实际结束时间',
  `start_time_window` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '开始时间窗口',
  `end_time_window` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '结束时间窗口',
  `reporter` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '变更代报人',
  `auto_start` tinyint(1) DEFAULT NULL COMMENT '是否自动开始',
  `status` enum('pending','running','aborting','succeed','failed','aborted','paused') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '状态',
  `config_hash` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '变更配置md5散列值',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `plan_start_time_idx` (`plan_start_time`) USING BTREE,
  KEY `plan_end_time_idx` (`plan_end_time`) USING BTREE,
  KEY `actual_start_time_idx` (`start_time`) USING BTREE,
  KEY `actual_end_time_idx` (`end_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更信息表';

-- ----------------------------
-- Table structure for change_auto_start
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_auto_start` (
  `change_id` bigint NOT NULL COMMENT '变更id',
  `target_time` timestamp(3) NULL DEFAULT NULL COMMENT '自动开始时间点',
  PRIMARY KEY (`change_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更定时自动开始表';

-- ----------------------------
-- Table structure for change_change_template
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_change_template` (
  `change_id` bigint NOT NULL COMMENT '变更id',
  `change_template_id` bigint NOT NULL COMMENT '变更模板id',
  PRIMARY KEY (`change_id`) USING BTREE,
  KEY `idx_change_template_id` (`change_template_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更引用变更模板关系表';

-- ----------------------------
-- Table structure for change_content
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_content` (
  `hash` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'hash值',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '文本内容',
  PRIMARY KEY (`hash`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更回复文本压缩表';

-- ----------------------------
-- Table structure for change_description
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_description` (
  `change_id` bigint NOT NULL COMMENT '变更id',
  `content_hash` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'hash值',
  `fcd` timestamp(3) NULL DEFAULT NULL COMMENT '创建时间',
  `fcu` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `lcd` timestamp(3) NULL DEFAULT NULL COMMENT '修改时间',
  `lcu` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`change_id`) USING BTREE,
  KEY `change_id_idx` (`change_id`) USING BTREE,
  KEY `content_hash_idx` (`content_hash`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更描述表';

-- ----------------------------
-- Table structure for change_file
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_file` (
  `change_id` bigint NOT NULL COMMENT '变更id',
  `file_id` bigint NOT NULL COMMENT '附件id',
  PRIMARY KEY (`change_id`,`file_id`) USING BTREE,
  KEY `idx_file_id` (`file_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更附件表';

-- ----------------------------
-- Table structure for change_param
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_param` (
  `id` bigint NOT NULL COMMENT '唯一标识',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '变量名',
  `mapping_method` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '映射方式',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更参数表';

-- ----------------------------
-- Table structure for change_sop
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_sop` (
  `id` bigint NOT NULL COMMENT '变更sop模板id',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
  `is_active` tinyint(1) DEFAULT NULL COMMENT '是否激活',
  `team_uuid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分组uuid，用于条件过滤',
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型，用于条件过滤',
  `fcu` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `fcd` timestamp(3) NULL DEFAULT NULL COMMENT '创建时间',
  `lcu` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `lcd` timestamp(3) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更SOP表';

-- ----------------------------
-- Table structure for change_sop_step
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_sop_step` (
  `uuid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '前端生成的uuid',
  `change_sop_id` bigint DEFAULT NULL COMMENT '变更sop模板id',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
  `plan_start_date` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '计划开始日期，yyyy-MM-dd',
  `start_time_window` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '时间窗口开始时间',
  `end_time_window` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '时间窗口结束时间',
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '编码',
  PRIMARY KEY (`uuid`) USING BTREE,
  KEY `chang_sop_id_idx` (`change_sop_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更SOP步骤表';

-- ----------------------------
-- Table structure for change_sop_step_content
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_sop_step_content` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '唯一主键id',
  `change_sop_id` bigint NOT NULL COMMENT '变更sop模板id',
  `change_sop_step_uuid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '变更sop模板步骤uuid',
  `content_hash` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'hash值',
  `fcd` timestamp(3) NULL DEFAULT NULL COMMENT '创建时间',
  `fcu` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `lcd` timestamp(3) NULL DEFAULT NULL COMMENT '修改时间',
  `lcu` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `change_step_id_idx` (`change_sop_step_uuid`) USING BTREE,
  KEY `change_id_idx` (`change_sop_id`) USING BTREE,
  KEY `content_hash_idx` (`content_hash`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=531 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更SOP表步骤描述表';

-- ----------------------------
-- Table structure for change_sop_step_file
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_sop_step_file` (
  `change_sop_id` bigint NOT NULL COMMENT '变更sop模板id',
  `change_sop_step_uuid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '变更sop模板步骤uuid',
  `file_id` bigint NOT NULL COMMENT '附件id',
  PRIMARY KEY (`change_sop_id`,`change_sop_step_uuid`,`file_id`) USING BTREE,
  KEY `idx_file_id` (`file_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更SOP表步骤附件表';

-- ----------------------------
-- Table structure for change_sop_step_param
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_sop_step_param` (
  `change_sop_id` bigint NOT NULL COMMENT 'sop模板id',
  `change_sop_step_uuid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'sop模板步骤uuid',
  `change_param_id` bigint NOT NULL COMMENT '变更参数id',
  PRIMARY KEY (`change_param_id`,`change_sop_step_uuid`) USING BTREE,
  KEY `change_sop_id_idx` (`change_sop_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更SOP表步骤参数表';

-- ----------------------------
-- Table structure for change_sop_step_team
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_sop_step_team` (
  `change_sop_id` bigint NOT NULL COMMENT '变更sop模板id',
  `change_sop_step_uuid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '变更sop模板步骤uuid',
  `team_uuid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '处理组uuid',
  PRIMARY KEY (`change_sop_step_uuid`) USING BTREE,
  KEY `change_id_idx` (`change_sop_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更SOP表步骤处理组表';

-- ----------------------------
-- Table structure for change_sop_step_user
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_sop_step_user` (
  `change_sop_id` bigint NOT NULL COMMENT '变更sop模板id',
  `change_sop_step_uuid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '变更sop模板步骤uuid',
  `user_uuid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '处理人uuid',
  PRIMARY KEY (`change_sop_step_uuid`) USING BTREE,
  KEY `change_id_idx` (`change_sop_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更SOP表步骤处理人表';

-- ----------------------------
-- Table structure for change_step
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_step` (
  `id` bigint NOT NULL COMMENT '唯一主键id',
  `uuid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '前端生成的uuid',
  `change_id` bigint DEFAULT NULL COMMENT '变更id',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
  `plan_start_date` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '计划开始日期，yyyy-MM-dd',
  `start_time` timestamp(3) NULL DEFAULT NULL COMMENT '实际开始时间',
  `end_time` timestamp(3) NULL DEFAULT NULL COMMENT '实际结束时间',
  `abort_time` timestamp(3) NULL DEFAULT NULL COMMENT '终止时间',
  `start_time_window` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '时间窗口开始时间',
  `end_time_window` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '时间窗口结束时间',
  `is_active` tinyint(1) DEFAULT NULL COMMENT '是否激活，未激活：0，已激活：1',
  `status` enum('unactived','pending','running','succeed','aborted') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '状态',
  `parent_id` bigint DEFAULT NULL COMMENT '父级id',
  `parent_uuid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '父级uuid',
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '编码',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `chang_id_idx` (`change_id`) USING BTREE,
  KEY `actual_start_time_idx` (`start_time`) USING BTREE,
  KEY `actual_end_time_idx` (`end_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更步骤信息表';

-- ----------------------------
-- Table structure for change_step_comment
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_step_comment` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `change_id` bigint NOT NULL COMMENT '变更id',
  `change_step_id` bigint NOT NULL COMMENT '变更步骤id',
  `content_hash` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '文本hash值',
  `file_id_list_hash` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '附件列表hash值',
  `fcd` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `fcu` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `lcd` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  `lcu` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `processtask_step_id_idx` (`change_step_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更步骤回复表';

-- ----------------------------
-- Table structure for change_step_content
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_step_content` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '唯一主键id',
  `change_id` bigint NOT NULL COMMENT '变更id',
  `change_step_id` bigint NOT NULL COMMENT '变更步骤id',
  `content_hash` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'hash值',
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型，描述、评论等',
  `fcd` timestamp(3) NULL DEFAULT NULL COMMENT '创建时间',
  `fcu` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `lcd` timestamp(3) NULL DEFAULT NULL COMMENT '修改时间',
  `lcu` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `change_step_id_idx` (`change_step_id`) USING BTREE,
  KEY `change_id_idx` (`change_id`) USING BTREE,
  KEY `content_hash_idx` (`content_hash`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=457 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更步骤描述表';

-- ----------------------------
-- Table structure for change_step_file
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_step_file` (
  `change_id` bigint NOT NULL COMMENT '变更id',
  `change_step_id` bigint NOT NULL COMMENT '变更步骤id',
  `file_id` bigint NOT NULL COMMENT '附件id',
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型，变更创建、变更评论等',
  PRIMARY KEY (`change_id`,`change_step_id`,`file_id`) USING BTREE,
  KEY `idx_file_id` (`file_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更步骤附件表';

-- ----------------------------
-- Table structure for change_step_pause_operate
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_step_pause_operate` (
  `change_id` bigint NOT NULL COMMENT '变更id',
  `change_step_id` bigint DEFAULT NULL COMMENT '变更步骤id',
  `fcu` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `fcd` timestamp(3) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`change_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更步骤暂停操作表';

-- ----------------------------
-- Table structure for change_step_team
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_step_team` (
  `change_id` bigint NOT NULL COMMENT '变更id',
  `change_step_id` bigint NOT NULL COMMENT '变更步骤id',
  `team_uuid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '处理组uuid',
  PRIMARY KEY (`change_step_id`) USING BTREE,
  KEY `change_id_idx` (`change_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更步骤处理组表';

-- ----------------------------
-- Table structure for change_step_user
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_step_user` (
  `change_id` bigint NOT NULL COMMENT '变更id',
  `change_step_id` bigint NOT NULL COMMENT '变更步骤id',
  `user_uuid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '处理人uuid',
  PRIMARY KEY (`change_step_id`) USING BTREE,
  KEY `change_id_idx` (`change_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更步骤处理人表';

-- ----------------------------
-- Table structure for change_template
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_template` (
  `id` bigint NOT NULL COMMENT '变更模板id',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
  `is_active` tinyint(1) DEFAULT NULL COMMENT '是否激活',
  `team_uuid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分组uuid，用于条件过滤',
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型，用于条件过滤',
  `fcu` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `fcd` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `lcu` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `lcd` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更模板表';

-- ----------------------------
-- Table structure for change_template_sop
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_template_sop` (
  `change_template_id` bigint NOT NULL COMMENT '变更模板id',
  `change_sop_id` bigint NOT NULL COMMENT '变更sop模板id',
  `sort` int NOT NULL COMMENT '排序',
  PRIMARY KEY (`change_template_id`,`change_sop_id`,`sort`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更模板引用SOP关系表';

-- ----------------------------
-- Table structure for change_user
-- ----------------------------
CREATE TABLE IF NOT EXISTS `change_user` (
  `change_id` bigint NOT NULL COMMENT '变更id',
  `user_uuid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '变更经理',
  PRIMARY KEY (`change_id`) USING BTREE,
  KEY `user_uuid_idx` (`user_uuid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='变更经理表';
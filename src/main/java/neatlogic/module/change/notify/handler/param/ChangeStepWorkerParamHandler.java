/*
Copyright(c) 2023 NeatLogic Co., Ltd. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package neatlogic.module.change.notify.handler.param;

import neatlogic.framework.change.constvalue.ChangeTriggerType;
import neatlogic.framework.notify.core.INotifyTriggerType;
import neatlogic.framework.process.dao.mapper.ProcessTaskMapper;
import neatlogic.framework.process.dto.ProcessTaskStepVo;
import neatlogic.framework.process.exception.process.ProcessStepHandlerNotFoundException;
import neatlogic.framework.process.notify.core.ProcessTaskNotifyParamHandlerBase;
import neatlogic.framework.process.stephandler.core.IProcessStepInternalHandler;
import neatlogic.framework.process.stephandler.core.ProcessStepInternalHandlerFactory;
import neatlogic.module.change.notify.constvalue.ChangeHandleNotifyParam;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author linbq
 * @since 2021/10/16 15:52
 **/
@Component
public class ChangeStepWorkerParamHandler extends ProcessTaskNotifyParamHandlerBase {

    @Resource
    private ProcessTaskMapper processTaskMapper;

    @Override
    public String getValue() {
        return ChangeHandleNotifyParam.CHANGESTEPWORKER.getValue();
    }

    @Override
    public Object getMyText(ProcessTaskStepVo processTaskStepVo, INotifyTriggerType notifyTriggerType) {
        if (!(notifyTriggerType instanceof ChangeTriggerType)) {
            return null;
        }
        JSONObject paramObj = processTaskStepVo.getParamObj();
        Long changeStepId = paramObj.getLong("changeStepId");
        if (changeStepId != null) {
            ProcessTaskStepVo stepVo = processTaskMapper.getProcessTaskStepBaseInfoById(processTaskStepVo.getId());
            IProcessStepInternalHandler handler = ProcessStepInternalHandlerFactory.getHandler(stepVo.getHandler());
            if (handler == null) {
                throw new ProcessStepHandlerNotFoundException(stepVo.getHandler());
            }
            Object handlerStepInfo = handler.getHandlerStepInitInfo(stepVo);
            if (handlerStepInfo != null) {
                JSONObject jsonObj = (JSONObject) JSON.toJSON(handlerStepInfo);
                JSONArray changeStepList = jsonObj.getJSONArray("changeStepList");
                if (CollectionUtils.isNotEmpty(changeStepList)) {
                    for (int i = 0; i < changeStepList.size(); i++) {
                        JSONObject changeStepObj = changeStepList.getJSONObject(i);
                        if (changeStepId.equals(changeStepObj.getLong("id"))) {
                            return changeStepObj.getJSONObject("workerVo").getString("name");
                        }
                    }
                }
            }
        }
        return null;
    }
}

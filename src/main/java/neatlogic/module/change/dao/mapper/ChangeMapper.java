package neatlogic.module.change.dao.mapper;

import neatlogic.framework.change.dto.*;
import neatlogic.framework.process.dto.ProcessTaskStepWorkerVo;
import neatlogic.framework.process.dto.ProcessTaskVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChangeMapper {
    Long getChangeIdByProcessTaskStepId(Long processTaskStepId);

    ChangeVo getChangeById(Long id);

    List<ChangeStepVo> getChangeStepListByChangeId(Long changeId);

    String getChangeUserByChangeId(Long changeId);

    String getChangeStepContentHashByChangeStepId(Long changeStepId);

    String getChangeContentByHash(String hash);

    List<Long> getChangeStepFileIdListByChangeStepId(Long changeStepId);

    /**
     * 根据fileId查询change_file、change_step_file 对应的changeId
     * @param fileId
     * @return
     */
    List<Long> getChangeIdListByFileId(Long fileId);

    ChangeStepVo getChangeStepById(Long id);

    int checkChangeIsExists(Long id);

    int checkChangeStepIsExists(Long id);

    ChangeStepUserVo getChangeStepUserByChangeStepId(Long changeStepId);

    List<ProcessTaskStepWorkerVo> getChangeStepWorkerList(Long processTaskStepId);

    ChangeStepTeamVo getChangeStepTeamByChangeStepId(Long changeStepId);

    ProcessTaskStepChangeVo getProcessTaskStepChangeHandleByChangeId(Long changeId);

    Long getChangeLockById(Long id);

    ChangeStepCommentVo getChangeStepCommentById(Long id);

    List<ChangeStepCommentVo> getChangeStepCommentListByChangeStepId(Long changeStepId);

    String getChangeConfigByProcessTaskStepId(Long processTaskStepId);

    Long getProcessTaskIdByChangeId(Long changeId);

    List<ProcessTaskVo> getProcessTaskVoListByChangeIdList(List<Long> changeIdList);

    ChangeAutoStartVo getChangeAutoStartByChangeId(Long changeId);

    List<ChangeAutoStartVo> getAllChangeAutoStart();

    String getChangeDescriptionContentHashByChangeId(Long changeId);

    List<Long> getChangeFileIdListByChangeId(Long changeId);

    Long getChangeTemplateIdByChangeId(Long changeId);

    Long getChangeStepPauseOperateChangeStepIdByChangeId(Long changeId);

    int insertChange(ChangeVo changeVo);

    int insertChangeStep(ChangeStepVo changeStepVo);

    int insertProcessTaskStepChangeCreate(ProcessTaskStepChangeVo processTaskStepChangeVo);

    int insertIgnoreProcessTaskStepChangeHandle(ProcessTaskStepChangeVo processTaskStepChangeVo);

    int insertChangeUser(@Param("changeId") Long changeId, @Param("userUuid") String userUuid);

    int replaceChangeContent(ChangeContentVo contentVo);

    int insertChangeStepContent(ChangeStepContentVo changeStepContentVo);

    int insertChangeStepFile(ChangeStepFileVo changeStepFileVo);

    int insertChangeStepUser(ChangeStepUserVo changeStepUserVo);

    int insertChangeStepTeam(ChangeStepTeamVo changeStepTeamVo);

    int insertChangeStepComment(ChangeStepCommentVo commentVo);

    int insertChangeAutoStart(ChangeAutoStartVo changeAutoStartVo);

    int insertChangeDescription(ChangeDescriptionVo changeDescriptionVo);

    int insertChangeFile(ChangeFileVo changeFileVo);

    int insertChangeChangeTemplate(@Param("changeId") Long changeId, @Param("changeTemplateId") Long changeTemplateId);

    int insertChangeStepPauseOperate(ChangeStepPauseOperateVo changeStepPauseOperateVo);

    int updateChangeStepById(ChangeStepVo changeStepVo);

    int updateChangeStatus(ChangeVo changeVo);

    int updateChangeStepStatus(ChangeStepVo changeStepVo);

    int recoverChangeStatus(ChangeVo changeVo);

    int recoverChangeStepStatus(ChangeStepVo changeStepVo);

    int updateChangeStepIsActive(ChangeStepVo changeStepVo);

    int updateChangeStepCommentById(ChangeStepCommentVo commentVo);

    int updateChangeById(ChangeVo changeVo);

    int updateChangeConfigByChangeId(@Param("changeId") Long changeId, @Param("config") String config);

    int deleteChangeStepUserByChangeStepId(Long changeStepId);

    int deleteChangeStepTeamByChangeStepId(Long changeStepId);

    int deleteChangeById(Long id);

    int deleteChangeUserByChangeId(Long id);

    int deleteChangeStepByChangeId(Long id);

    int deleteChangeStepUserByChangeId(Long id);

    int deleteChangeStepTeamByChangeId(Long id);

    int deleteChangeStepContentByChangeId(Long id);

    int deleteChangeStepFileByChangeId(Long id);

    int deleteChangeStepCommentById(Long id);

    int deleteChangeStepCommentByChangeId(Long changeId);

    int deleteChangeAutoStartByChangeId(Long changeId);

    int deleteChangeDescriptionByChangeId(Long changeId);

    int deleteChangeFileByChangeId(Long changeId);

    int deleteChangeStepFileByFileId(Long fileId);

    int deleteChangeChangeTemplateByChangeId(Long id);

    int deleteChangeStepPauseOperateByChangeId(Long changeId);

    int deleteProcessTaskStepChangeHandleByChangeId(Long changeId);
}

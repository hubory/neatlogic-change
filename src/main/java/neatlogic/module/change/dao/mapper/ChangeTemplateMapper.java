package neatlogic.module.change.dao.mapper;

import neatlogic.framework.change.dto.*;
import neatlogic.framework.common.dto.BasePageVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface ChangeTemplateMapper {

    public ChangeSopVo getChangeSopById(Long id);
    
    public int checkChangeSopIsExists(Long changeSopId);

    public List<ChangeSopStepVo> getChangeSopStepListByChangeSopId(Long changeSopId);

    public String getChangeSopStepContentHashByChangeSopStepUuid(String changeSopStepUuid);

    public List<Long> getChangeSopStepFileIdListByChangeSopStepUuid(String changeSopStepUuid);

    public ChangeSopStepUserVo getChangeSopStepUserByChangeSopStepUuid(String changeSopStepUuid);

    public ChangeSopStepTeamVo getChangeSopStepTeamByChangeSopStepUuid(String changeSopStepUuid);

    public List<ChangeSopVo> getChangeSopList(ChangeSopVo changeSopVo);

    public int getChangeSopCount(ChangeSopVo changeSopVo);

    public List<String> getChangeSopTypeList(BasePageVo basePageVo);

    public int checkChangeTemplateNameIsRepeat(ChangeTemplateVo changeTemplateVo);

    public ChangeTemplateVo getChangeTemplateById(Long id);

    public int checkChangeTemplateIsExists(Long changeTemplateId);
    
    public int getChangeSopTypeCount(BasePageVo basePageVo);

    public int checkChangeSopNameIsRepeat(ChangeSopVo changeSopVo);

    public List<Long> getChangeSopIdListByChangeTemplateId(Long changeTemplateId);

    public int getChangeTemplateCount(ChangeTemplateVo changeTemplateVo);

    public List<ChangeTemplateVo> getChangeTemplateList(ChangeTemplateVo changeTemplateVo);

    public int getChangeTemplateTypeCount(BasePageVo basePageVo);

    public List<String> getChangeTemplateTypeList(BasePageVo basePageVo);

    public ChangeParamVo getChangeParamByName(String name);

    public int getChangeParamCount(BasePageVo basePageVo);

    public List<ChangeParamVo> getChangeParamList(BasePageVo basePageVo);

    public int checkChangeParamIsExists(Long id);

    public int checkChangeParamNameIsRepeat(ChangeParamVo changeParamVo);

    public ChangeParamVo getChangeParamById(Long id);

    public List<String> getChangeSopStepUuidListByChangeParamId(Long changeParamId);

    public List<String> getChangeSopStepContentHashListByUuidList(List<String> changeSopStepUuidList);

    public List<ChangeSopStepVo> getChangeSopStepListByUuidList(List<String> changeSopStepUuidList);

    public List<Long> getChangeParamIdListByChangeSopId(Long changeSopId);

    public List<ChangeParamVo> getChangeParamListByIdSet(Set<Long> changeParamIdSet);

    public List<Long> getChangeTemplateIdListByChangeSopId(Long changeSopId);

    public List<ChangeTemplateVo> getChangeTemplateListByIdList(List<Long> idList);

    public int insertChangeSop(ChangeSopVo changeSopVo);

    public int insertChangeSopStep(ChangeSopStepVo changeSopStepVo);

    public int insertChangeSopStepContent(ChangeSopStepContentVo changeSopStepContentVo);

    public int insertChangeSopStepFile(ChangeSopStepFileVo changeSopStepFileVo);

    public int insertChangeSopStepUser(ChangeSopStepUserVo changeSopStepUserVo);

    public int insertChangeSopStepTeam(ChangeSopStepTeamVo changeSopStepTeamVo);

    public int insertChangeTemplate(ChangeTemplateVo changeTemplateVo);

    public int insertChangeTemplateSop(ChangeTemplateSopVo changeTemplateSopVo);

    public int insertChangeParam(ChangeParamVo changeParamVo);

    public int insertChangeSopStepParam(ChangeSopStepParamVo changeSopStepParamVo);

    public int updateChangeTemplateIsActiveById(ChangeTemplateVo changeTemplateVo);

    public int updateChangeSopById(ChangeSopVo changeSopVo);

    public int updateChangeTemplateById(ChangeTemplateVo changeTemplateVo);

    public int updateChangeParamById(ChangeParamVo changeParamVo);

    public int updateChangeSopStepContentByHashList(@Param("hashList") List<String> changeSopStepContentHashList,
        @Param("oldParamName") String oldParamName, @Param("newParamName") String newParamName);

    public int deleteChangeSopById(Long id);

    public int deleteChangeSopStepByChangeSopId(Long id);

    public int deleteChangeSopStepUserByChangeSopId(Long id);

    public int deleteChangeSopStepTeamByChangeSopId(Long id);

    public int deleteChangeSopStepContentByChangeSopId(Long id);

    public int deleteChangeSopStepFileByChangeSopId(Long id);

    public int deleteChangeTemplateById(Long id);

    public int deleteChangeTemplateSopByChangeTemplateId(Long id);

    public int deleteChangeParamById(Long id);

    public int deleteChangeSopStepParamByChangeSopId(Long chagneSopId);

    public int deleteChangeSopStepParamByChangeParamId(Long changeParamId);

    public int checkChangeSopStepFileIdIsExists(Long fileId);
}

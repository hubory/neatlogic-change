/*
 * Copyright(c) 2023 NeatLogic Co., Ltd. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package neatlogic.module.change.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class TestChangeStepProcess {

	private static List<String> allCodeList = new ArrayList<>();
	
	private static List<String> unActiveList = new ArrayList<>();
	private static List<String> pendingList = new ArrayList<>();
	private static List<String> runningList = new ArrayList<>();
	private static List<String> finishList = new ArrayList<>();
	private static List<String> abortedList = new ArrayList<>();
	
	static {
		allCodeList.add("1.A");
		allCodeList.add("1.B");
		
		allCodeList.add("2.A.1");
		allCodeList.add("2.A.2");
		allCodeList.add("2.B.1");
		allCodeList.add("2.B.2");
		
		allCodeList.add("3.A");
		allCodeList.add("3.B");
		allCodeList.add("3.C.1");
		allCodeList.add("3.C.2.A");
		allCodeList.add("3.C.2.B.1");
		allCodeList.add("3.C.2.B.2");
		allCodeList.add("3.C.2.B.3");
		allCodeList.add("3.C.2.B.4");
		allCodeList.add("3.C.2.C");
		allCodeList.add("3.C.2.D");
		allCodeList.add("3.C.3");
		allCodeList.add("3.D");
		allCodeList.add("3.E");
		allCodeList.add("4");
		allCodeList.add("5");
	}
	public static void main(String[] args) {
		allCodeList.sort((e1, e2) -> e1.compareToIgnoreCase(e2));
		unActiveList.addAll(allCodeList);
		printStepList();		
		activeStep();
		Scanner sc = null;
		try {
			sc = new Scanner(System.in);
			while(true) {
				System.out.print("请输入：");
				String action = sc.nextLine();
				if("end".equalsIgnoreCase(action)) {
					break;
				}else {
					String code = action.substring(2).toUpperCase();
					if(action.startsWith("s-")){
						if(pendingList.remove(code)) {
							runningList.add(code);
							activeStep();
						}else {
                            //System.out.println(code + "不能执行“开始”操作");
						}
					}else if(action.startsWith("f-")){
						
						if(runningList.remove(code)) {
							finishList.add(code);
							activeStep();
						}else {
                            //System.out.println(code + "不能执行“完成”操作");
						}
					}else if(action.startsWith("a-")){
						if(pendingList.remove(code) || runningList.remove(code)) {
							abortedList.add(code);
							activeStep();
						}else {
                            //System.out.println(code + "不能执行“取消”操作");
						}
					}else {
                        //System.out.println("没有这种操作：" + action);
					}
				}	
				if(unActiveList.isEmpty() && pendingList.isEmpty() && runningList.isEmpty()) {
					break;
				}
			}
		}finally {
			if(sc != null) {
				sc.close();
			}
		}

        //System.out.println("---------------end----------------------");
	}
	
	private static void printStepList() {
		for(String code : allCodeList) {
			if(unActiveList.contains(code)) {
				System.out.println(0 + "\t" + "pending" + "\t" + code);
			}else if(pendingList.contains(code)) {
				System.out.println(1 + "\t" + "pending" + "\t" + code);
			}else if(runningList.contains(code)) {
				System.out.println(1 + "\t" + "running" + "\t" + code);
			}else if(finishList.contains(code)) {
				System.out.println(2 + "\t" + "suecced" + "\t" + code);
			}else if(abortedList.contains(code)) {
				System.out.println(-1 + "\t" + "aborted" + "\t" + code);
			}
		}
	}
	/**
	 * 
	* @Time:2020年8月4日
	* @Description: 找出能激活的步骤，激活
	* @return List<String> 本次激活的步骤列表
	 */
	private static void activeStep() {
		System.out.println("---------------本次激活的步骤列表---------------");
		Iterator<String> iterator = unActiveList.iterator();
		while(iterator.hasNext()) {
			String code = iterator.next();
			if(isActivatable(code)) {
				iterator.remove();
				pendingList.add(code);
				System.out.println(code);
			}
		}
		System.out.println("---------------本次激活的步骤列表---------------");
		printStepList();
	}

	private static boolean isActivatable(String code) {
		for(String str : pendingList) {
			if(!compare(code, str)) {
				return false;
			}
		}
		for(String str : runningList) {
			if(!compare(code, str)) {
				return false;
			}
		}
		return true;
	}
	/**
	 * 
	* @Time:2020年8月4日
	* @Description: TODO 
	* @param unActiveCode 判断是否可以激活的变更步骤编码
	* @param runningCode 待处理或处理中的变更步骤编码
	* @return boolean 返回false，说明unActiveCode步骤需要等待runningCode步骤完成后才能激活
	 */
	private static boolean compare(String unActiveCode, String runningCode) {
		if(unActiveCode.equals(runningCode)) {
			return true;
		}
		int min = unActiveCode.length() > runningCode.length() ? runningCode.length() : unActiveCode.length();
		for(int i = 0; i < min; i++) {
			char c1 = unActiveCode.charAt(i);
			char c2 = runningCode.charAt(i);
			if(c1 != c2) {
				if(Character.isDigit(c1) && Character.isDigit(c2)) {//数字
					if(c1 > c2) {
						//相同位置的数字，如果未激活步骤大于处理中步骤，说明未激活步骤需要等待处理中步骤完成才能激活
						return false;
					}
				}else {//字母不相等时，说明两个步骤是并行关系，后面的数字不需要比较了
					return true;
				}
			}
		}
		return true;
	}
}

package neatlogic.module.change.api;

import java.util.List;

import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.process.auth.PROCESS_BASE;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.module.change.dao.mapper.ChangeMapper;
import neatlogic.framework.change.dto.ChangeStepCommentVo;
import neatlogic.framework.change.dto.ChangeStepVo;
import neatlogic.framework.change.exception.ChangeStepNotFoundException;
import neatlogic.module.change.service.ChangeService;

@Service
@AuthAction(action = PROCESS_BASE.class)
@OperationType(type = OperationTypeEnum.SEARCH)
public class ChangeStepCommentListApi extends PrivateApiComponentBase {

	@Autowired
	private ChangeMapper changeMapper;
	
	@Autowired
	private ChangeService changeService;

	@Override
	public String getToken() {
		return "change/step/comment/list";
	}

	@Override
	public String getName() {
		return "查询变更步骤回复列表";
	}

	@Override
	public String getConfig() {
		return null;
	}
	
	@Input({
		@Param(name = "changeStepId", type = ApiParamType.LONG, isRequired = true, desc = "变更步骤id")
	})
	@Output({
		@Param(name = "commentList", explode=ChangeStepCommentVo[].class, desc = "回复列表")
	})
	@Description(desc = "回复变更步骤")
	@Override
	public Object myDoService(JSONObject jsonObj) throws Exception {
		Long changeStepId = jsonObj.getLong("changeStepId");
		ChangeStepVo changeStep = changeMapper.getChangeStepById(changeStepId);
		if(changeStep == null) {
			throw new ChangeStepNotFoundException(changeStepId);
		}
		List<ChangeStepCommentVo> changeStepCommentList = changeService.getChangeStepCommentListByChangeStepId(changeStepId);
		return changeStepCommentList;
	}

}

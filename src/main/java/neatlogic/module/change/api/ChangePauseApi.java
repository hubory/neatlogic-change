package neatlogic.module.change.api;

import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.change.constvalue.*;
import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.common.constvalue.GroupSearch;
import neatlogic.framework.process.auth.PROCESS_BASE;
import neatlogic.framework.process.constvalue.ProcessTaskAuditDetailType;
import neatlogic.framework.process.dao.mapper.ProcessTaskMapper;
import neatlogic.framework.process.dto.ProcessTaskStepVo;
import neatlogic.framework.process.stephandler.core.IProcessStepHandlerUtil;
import neatlogic.framework.restful.annotation.Description;
import neatlogic.framework.restful.annotation.Input;
import neatlogic.framework.restful.annotation.OperationType;
import neatlogic.framework.restful.annotation.Param;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.module.change.dao.mapper.ChangeMapper;
import neatlogic.framework.change.dto.ChangeVo;
import neatlogic.framework.change.dto.ProcessTaskStepChangeVo;
import neatlogic.framework.change.exception.ChangeHandleHasNotStartedException;
import neatlogic.framework.change.exception.ChangeNoPermissionException;
import neatlogic.framework.change.exception.ChangeNotFoundException;
import neatlogic.module.change.service.ChangeService;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service
@Transactional
@AuthAction(action = PROCESS_BASE.class)
@OperationType(type = OperationTypeEnum.OPERATE)
public class ChangePauseApi extends PrivateApiComponentBase {

	@Autowired
	private ChangeMapper changeMapper;
	
	@Autowired
	private ChangeService changeService;
	
    @Autowired
    private ProcessTaskMapper processTaskMapper;

    @Autowired
    private IProcessStepHandlerUtil IProcessStepHandlerUtil;

	@Override
	public String getToken() {
		return "change/pause";
	}

	@Override
	public String getName() {
		return "暂停变更";
	}

	@Override
	public String getConfig() {
		return null;
	}

	@Input({
		@Param(name = "changeId", type = ApiParamType.LONG, isRequired = true, desc = "变更id"),
		@Param(name = "content", type = ApiParamType.STRING, desc = "描述"),
		@Param(name = "source", type = ApiParamType.STRING, defaultValue = "pc", desc = "来源")
	})
	@Description(desc = "变更暂停接口")
	@Override
	public Object myDoService(JSONObject jsonObj) throws Exception {
		Long changeId = jsonObj.getLong("changeId");
		ChangeVo changeVo = changeMapper.getChangeById(changeId);
		if(changeMapper.checkChangeIsExists(changeId) == 0) {
			throw new ChangeNotFoundException(changeId);
		}
		/** 获取当前变更锁 **/
		changeMapper.getChangeLockById(changeId);
		String owner = changeMapper.getChangeUserByChangeId(changeId);
		if(StringUtils.isNotBlank(owner)) {
			changeVo.setOwner(GroupSearch.USER.getValuePlugin() + owner);
		}
		changeVo.setChangeStepList(changeMapper.getChangeStepListByChangeId(changeId));
		if(!changeService.isPauseableChange(changeVo)) {
			throw new ChangeNoPermissionException(ChangeOperationType.PAUSECHANGE.getText());
		}
		/** 暂停变更时，将变更状态设置为“已取消”，变更步骤状态不变 **/
		changeVo.setStatus(ChangeStatus.PAUSED.getValue());
		changeMapper.updateChangeStatus(changeVo);       

		/** 生成活动 **/
		ProcessTaskStepChangeVo processTaskStepChangeVo = changeMapper.getProcessTaskStepChangeHandleByChangeId(changeId);
		if(processTaskStepChangeVo == null) {
			throw new ChangeHandleHasNotStartedException();
		}

		ProcessTaskStepVo currentProcessTaskStepVo = processTaskMapper.getProcessTaskStepBaseInfoById(processTaskStepChangeVo.getProcessTaskStepId());
		String content = jsonObj.getString("content");
        /** 变更暂停提醒 **/
		IProcessStepHandlerUtil.saveStepRemind(currentProcessTaskStepVo, currentProcessTaskStepVo.getId(), content, ChangeRemindType.PAUSECHANGE);
		jsonObj.remove("content");
		jsonObj.put(ProcessTaskAuditDetailType.CAUSE.getParamName(), content);
		currentProcessTaskStepVo.getParamObj().putAll(jsonObj);
		IProcessStepHandlerUtil.audit(currentProcessTaskStepVo, ChangeAuditType.PAUSECHANGE);
		IProcessStepHandlerUtil.notify(currentProcessTaskStepVo, ChangeTriggerType.PAUSECHANGE);
		IProcessStepHandlerUtil.action(currentProcessTaskStepVo, ChangeTriggerType.PAUSECHANGE);
		return null;
	}

}

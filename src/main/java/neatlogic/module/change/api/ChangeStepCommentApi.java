package neatlogic.module.change.api;

import neatlogic.framework.asynchronization.threadlocal.UserContext;
import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.change.dto.*;
import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.exception.file.FileNotFoundException;
import neatlogic.framework.file.dao.mapper.FileMapper;
import neatlogic.framework.process.auth.PROCESS_BASE;
import neatlogic.framework.process.dto.ProcessTaskStepVo;
import neatlogic.framework.process.stephandler.core.IProcessStepHandlerUtil;
import neatlogic.framework.restful.annotation.*;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.framework.change.constvalue.ChangeAuditType;
import neatlogic.framework.change.constvalue.ChangeOperationType;
import neatlogic.framework.change.constvalue.ChangeTriggerType;
import neatlogic.module.change.dao.mapper.ChangeMapper;
import neatlogic.framework.change.exception.ChangeHandleHasNotStartedException;
import neatlogic.framework.change.exception.ChangeNoPermissionException;
import neatlogic.framework.change.exception.ChangeStepNotFoundException;
import neatlogic.module.change.service.ChangeService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
@AuthAction(action = PROCESS_BASE.class)
@OperationType(type = OperationTypeEnum.OPERATE)
public class ChangeStepCommentApi extends PrivateApiComponentBase {

	@Autowired
	private ChangeMapper changeMapper;
	
	@Autowired
	private FileMapper fileMapper;
	
	@Autowired
	private ChangeService changeService;

	@Autowired
	private IProcessStepHandlerUtil IProcessStepHandlerUtil;

	@Override
	public String getToken() {
		return "change/step/comment";
	}

	@Override
	public String getName() {
		return "回复变更步骤";
	}

	@Override
	public String getConfig() {
		return null;
	}

	@Input({
		@Param(name = "changeStepId", type = ApiParamType.LONG, isRequired = true, desc = "变更步骤id"),
		@Param(name = "content", type = ApiParamType.STRING, desc = "描述"),
		@Param(name = "fileIdList", type=ApiParamType.JSONARRAY, desc = "附件id列表"),
		@Param(name = "source", type = ApiParamType.STRING, defaultValue = "pc", desc = "来源")
	})
	@Output({
		@Param(name = "id", type = ApiParamType.LONG, desc = "回复id")
	})
	@Description(desc = "回复变更步骤")
	@Override
	public Object myDoService(JSONObject jsonObj) throws Exception {
		Long changeStepId = jsonObj.getLong("changeStepId");
		ChangeStepVo changeStep = changeMapper.getChangeStepById(changeStepId);
		if(changeStep == null) {
			throw new ChangeStepNotFoundException(changeStepId);
		}
		/** 获取当前变更锁 **/
		changeMapper.getChangeLockById(changeStep.getChangeId());
		
		ChangeVo changeVo = changeMapper.getChangeById(changeStep.getChangeId());
		changeService.getChangeStepDetail(changeStep);
		if(!changeService.isCommentableChangeStep(changeVo, changeStep)) {
			throw new ChangeNoPermissionException(ChangeOperationType.COMMENTCHANGESTEP.getText());
		}
		
		ChangeStepCommentVo comment = null;
		
		/** 描述内容 **/
		String content = jsonObj.getString("content");
		if (StringUtils.isNotBlank(content)) {
			comment = new ChangeStepCommentVo();
			ChangeContentVo contentVo = new ChangeContentVo(content);
			changeMapper.replaceChangeContent(contentVo);
			comment.setContentHash(contentVo.getHash());
		}
		/** 附件 **/
		List<Long> fileIdList = JSON.parseArray(JSON.toJSONString(jsonObj.getJSONArray("fileIdList")), Long.class);
		if(CollectionUtils.isNotEmpty(fileIdList)) {
			for(Long fileId : fileIdList) {
				if(fileMapper.getFileById(fileId) == null) {
					throw new FileNotFoundException(fileId);
				}
				changeMapper.insertChangeStepFile(new ChangeStepFileVo(changeStep.getChangeId(), changeStepId, fileId));
			}
			if(comment == null) {
				comment = new ChangeStepCommentVo();
			}
			ChangeContentVo fileContent = new ChangeContentVo(JSON.toJSONString(fileIdList));
			changeMapper.replaceChangeContent(fileContent);
			comment.setFileIdListHash(fileContent.getHash());
		}
		if(comment != null) {
			comment.setChangeId(changeStep.getChangeId());
			comment.setChangeStepId(changeStepId);
			comment.setFcu(UserContext.get().getUserUuid(true));
			changeMapper.insertChangeStepComment(comment);
		}
		
		/** 生成活动 **/
		ProcessTaskStepChangeVo processTaskStepChangeVo = changeMapper.getProcessTaskStepChangeHandleByChangeId(changeStep.getChangeId());
		if(processTaskStepChangeVo == null) {
			throw new ChangeHandleHasNotStartedException();
		}

		ProcessTaskStepVo currentProcessTaskStepVo = new ProcessTaskStepVo();
		currentProcessTaskStepVo.setProcessTaskId(processTaskStepChangeVo.getProcessTaskId());
		currentProcessTaskStepVo.setId(processTaskStepChangeVo.getProcessTaskStepId());
		currentProcessTaskStepVo.getParamObj().put("changeStepName", changeStep.getName());
		currentProcessTaskStepVo.getParamObj().putAll(jsonObj);
		IProcessStepHandlerUtil.audit(currentProcessTaskStepVo, ChangeAuditType.COMMENTCHANGESTEP);
		IProcessStepHandlerUtil.notify(currentProcessTaskStepVo, ChangeTriggerType.COMMENTCHANGESTEP);
		IProcessStepHandlerUtil.action(currentProcessTaskStepVo, ChangeTriggerType.COMMENTCHANGESTEP);
		return comment.getId();
	}

}

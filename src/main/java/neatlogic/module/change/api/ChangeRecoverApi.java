package neatlogic.module.change.api;


import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.change.constvalue.*;
import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.common.constvalue.GroupSearch;
import neatlogic.framework.process.auth.PROCESS_BASE;
import neatlogic.framework.process.dao.mapper.ProcessTaskMapper;
import neatlogic.framework.process.dto.ProcessTaskStepRemindVo;
import neatlogic.framework.process.dto.ProcessTaskStepVo;
import neatlogic.framework.process.stephandler.core.IProcessStepHandlerUtil;
import neatlogic.framework.restful.annotation.Description;
import neatlogic.framework.restful.annotation.Input;
import neatlogic.framework.restful.annotation.OperationType;
import neatlogic.framework.restful.annotation.Param;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.module.change.dao.mapper.ChangeMapper;
import neatlogic.framework.change.dto.ChangeStepVo;
import neatlogic.framework.change.dto.ChangeVo;
import neatlogic.framework.change.dto.ProcessTaskStepChangeVo;
import neatlogic.framework.change.exception.ChangeHandleHasNotStartedException;
import neatlogic.framework.change.exception.ChangeNoPermissionException;
import neatlogic.framework.change.exception.ChangeNotFoundException;
import neatlogic.module.change.service.ChangeService;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service
@Transactional
@AuthAction(action = PROCESS_BASE.class)
@OperationType(type = OperationTypeEnum.OPERATE)
public class ChangeRecoverApi extends PrivateApiComponentBase {

	@Autowired
	private ChangeMapper changeMapper;
	
	@Autowired
	private ChangeService changeService;
	
	@Autowired
    private ProcessTaskMapper processTaskMapper;
	@Autowired
    private IProcessStepHandlerUtil IProcessStepHandlerUtil;

	@Override
	public String getToken() {
		return "change/recover";
	}

	@Override
	public String getName() {
		return "恢复变更";
	}

	@Override
	public String getConfig() {
		return null;
	}

	@Input({
		@Param(name = "changeId", type = ApiParamType.LONG, isRequired = true, desc = "变更id"),
		@Param(name = "content", type = ApiParamType.STRING, desc = "描述"),
		@Param(name = "source", type = ApiParamType.STRING, defaultValue = "pc", desc = "来源")
	})
	@Description(desc = "恢复变更")
	@Override
	public Object myDoService(JSONObject jsonObj) throws Exception {
		Long changeId = jsonObj.getLong("changeId");
		ChangeVo changeVo = changeMapper.getChangeById(changeId);
		if(changeVo == null) {
			throw new ChangeNotFoundException(changeId);
		}
		/** 获取当前变更锁 **/
		changeMapper.getChangeLockById(changeId);
		
		String owner = changeMapper.getChangeUserByChangeId(changeId);
		if(StringUtils.isNotBlank(owner)) {
			changeVo.setOwner(GroupSearch.USER.getValuePlugin() + owner);
		}
		changeVo.setChangeStepList(changeMapper.getChangeStepListByChangeId(changeId));
		if(!changeService.isRecoverableChange(changeVo)) {
			throw new ChangeNoPermissionException(ChangeOperationType.RECOVERCHANGE.getText());
		}
		/** 恢复变更时，将变更状态设置为“处理中” **/
		changeVo.setStatus(ChangeStatus.RUNNING.getValue());
		changeMapper.recoverChangeStatus(changeVo);
		
		/** 如果是变更步骤处理人暂停变更，将对应的变更步骤状态恢复 **/
		Long changeStepId = changeMapper.getChangeStepPauseOperateChangeStepIdByChangeId(changeId);
		if(changeStepId != null) {
		    ChangeStepVo changeStepVo = changeMapper.getChangeStepById(changeStepId);
		    if(changeStepVo.getStartTime() != null) {
		        changeStepVo.setStatus(ChangeStatus.RUNNING.getValue());
		    }else {
		        changeStepVo.setStatus(ChangeStatus.PENDING.getValue());
		    }
            changeMapper.updateChangeStepStatus(changeStepVo);
            changeMapper.deleteChangeStepPauseOperateByChangeId(changeId);
		}
		
		/** 生成活动 **/
		ProcessTaskStepChangeVo processTaskStepChangeVo = changeMapper.getProcessTaskStepChangeHandleByChangeId(changeId);
		if(processTaskStepChangeVo == null) {
			throw new ChangeHandleHasNotStartedException();
		}
		ProcessTaskStepVo currentProcessTaskStepVo = processTaskMapper.getProcessTaskStepBaseInfoById(processTaskStepChangeVo.getProcessTaskStepId());
		currentProcessTaskStepVo.getParamObj().putAll(jsonObj);
		IProcessStepHandlerUtil.audit(currentProcessTaskStepVo, ChangeAuditType.RECOVERCHANGE);
		IProcessStepHandlerUtil.notify(currentProcessTaskStepVo, ChangeTriggerType.RECOVERCHANGE);
		IProcessStepHandlerUtil.action(currentProcessTaskStepVo, ChangeTriggerType.RECOVERCHANGE);
		processTaskMapper.deleteProcessTaskStepRemind(new ProcessTaskStepRemindVo(processTaskStepChangeVo.getProcessTaskStepId(), ChangeRemindType.PAUSECHANGE.getValue()));
		return null;
	}

}

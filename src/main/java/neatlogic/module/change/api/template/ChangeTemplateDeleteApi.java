package neatlogic.module.change.api.template;

import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.Description;
import neatlogic.framework.restful.annotation.Input;
import neatlogic.framework.restful.annotation.OperationType;
import neatlogic.framework.restful.annotation.Param;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.module.change.auth.label.CHANGE_MODIFY;
import neatlogic.module.change.dao.mapper.ChangeTemplateMapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service
@Transactional
@OperationType(type = OperationTypeEnum.DELETE)
@AuthAction(action = CHANGE_MODIFY.class)
public class ChangeTemplateDeleteApi extends PrivateApiComponentBase {
    
    @Autowired
    private ChangeTemplateMapper changeTemplateMapper;

    @Override
    public String getToken() {
        return "change/template/delete";
    }

    @Override
    public String getName() {
        return "删除变更模板";
    }

    @Override
    public String getConfig() {
        return null;
    }
    
    @Input({
        @Param(name = "changeTemplateId", type = ApiParamType.LONG, isRequired = true, desc = "变更模板id")
    })
    @Description(desc = "删除变更模板")
    @Override
    public Object myDoService(JSONObject jsonObj) throws Exception {
        Long changeTemplateId = jsonObj.getLong("changeTemplateId");
        if(changeTemplateMapper.checkChangeTemplateIsExists(changeTemplateId) > 0) {
            changeTemplateMapper.deleteChangeTemplateById(changeTemplateId);
            changeTemplateMapper.deleteChangeTemplateSopByChangeTemplateId(changeTemplateId);
        }
        return null;
    }

}

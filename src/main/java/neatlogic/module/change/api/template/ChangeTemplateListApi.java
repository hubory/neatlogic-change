package neatlogic.module.change.api.template;

import java.util.ArrayList;
import java.util.List;

import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.process.auth.PROCESS_BASE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.common.dto.BasePageVo;
import neatlogic.framework.common.util.PageUtil;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.Description;
import neatlogic.framework.restful.annotation.Input;
import neatlogic.framework.restful.annotation.OperationType;
import neatlogic.framework.restful.annotation.Output;
import neatlogic.framework.restful.annotation.Param;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.module.change.dao.mapper.ChangeTemplateMapper;
import neatlogic.framework.change.dto.ChangeTemplateVo;
@Service
@AuthAction(action = PROCESS_BASE.class)
@OperationType(type = OperationTypeEnum.SEARCH)
public class ChangeTemplateListApi extends PrivateApiComponentBase {
    
    @Autowired
    private ChangeTemplateMapper changeTemplateMapper;

    @Override
    public String getToken() {
        return "change/template/list";
    }

    @Override
    public String getName() {
        return "查询变更模板列表";
    }

    @Override
    public String getConfig() {
        return null;
    }
    
    @Input({
        @Param(name = "keyword", type = ApiParamType.STRING, desc = "关键字，模糊匹配"),
        @Param(name = "authority", type = ApiParamType.STRING, desc = "分组uuid，格式：team#uuid"),
        @Param(name = "type", type = ApiParamType.STRING, desc = "变更sop模板类型"),
        @Param(name = "isActive", type = ApiParamType.ENUM, rule = "0,1", desc = "是否激活"),
        @Param(name = "needPage", type = ApiParamType.BOOLEAN, desc = "是否分页"),
        @Param(name = "currentPage", type = ApiParamType.INTEGER, desc = "当前页数"),
        @Param(name = "pageSize", type = ApiParamType.INTEGER, desc = "每页条数")
    })
    @Output({
        @Param(name = "changeTemplateList", explode = ChangeTemplateVo[].class, desc = "变更模板列表"),
        @Param(explode = BasePageVo.class)
    })
    @Description(desc = "查询变更模板列表")
    @Override
    public Object myDoService(JSONObject jsonObj) throws Exception {
        JSONObject resultObj = new JSONObject();
        resultObj.put("changeTemplateList", new ArrayList<>());
        int pageCount = 0;
        ChangeTemplateVo changeTemplateVo = JSON.toJavaObject(jsonObj, ChangeTemplateVo.class);
        if(changeTemplateVo.getNeedPage()) {
            int rowNum = changeTemplateMapper.getChangeTemplateCount(changeTemplateVo);
            pageCount = PageUtil.getPageCount(rowNum, changeTemplateVo.getPageSize());
            resultObj.put("currentPage", changeTemplateVo.getCurrentPage());
            resultObj.put("pageSize", changeTemplateVo.getPageSize());
            resultObj.put("pageCount", pageCount);
            resultObj.put("rowNum", rowNum);
        }
        if(!changeTemplateVo.getNeedPage() || changeTemplateVo.getCurrentPage() <= pageCount) {
            List<ChangeTemplateVo> changeTemplateList = changeTemplateMapper.getChangeTemplateList(changeTemplateVo);
            resultObj.put("changeTemplateList", changeTemplateList);
        }       
        return resultObj;
    }

}

package neatlogic.module.change.api.template;

import neatlogic.framework.asynchronization.threadlocal.UserContext;
import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.dto.FieldValidResultVo;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.*;
import neatlogic.framework.restful.core.IValid;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.module.change.auth.label.CHANGE_MODIFY;
import neatlogic.module.change.dao.mapper.ChangeTemplateMapper;
import neatlogic.framework.change.dto.ChangeTemplateSopVo;
import neatlogic.framework.change.dto.ChangeTemplateVo;
import neatlogic.framework.change.exception.ChangeTemplateNameRepeatException;
import neatlogic.framework.change.exception.ChangeTemplateNotFoundException;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
@OperationType(type = OperationTypeEnum.CREATE)
@AuthAction(action = CHANGE_MODIFY.class)
public class ChangeTemplateCopyApi extends PrivateApiComponentBase {
    
    @Autowired
    private ChangeTemplateMapper changeTemplateMapper;

    @Override
    public String getToken() {
        return "change/template/copy";
    }

    @Override
    public String getName() {
        return "复制变更模板";
    }

    @Override
    public String getConfig() {
        return null;
    }
    
    @Input({
        @Param(name = "changeTemplateId", type = ApiParamType.LONG, isRequired = true, desc = "变更模板id"),
        @Param(name = "name", type = ApiParamType.STRING, isRequired = true, desc = "名称")
    })
    @Output({
        @Param(name = "id", type = ApiParamType.LONG, desc = "变更模板id")
    })
    @Description(desc = "复制变更模板")
    @Override
    public Object myDoService(JSONObject jsonObj) throws Exception {
        Long changeTemplateId = jsonObj.getLong("changeTemplateId");
        String name = jsonObj.getString("name");
        ChangeTemplateVo changeTemplate = new ChangeTemplateVo();
        changeTemplate.setName(name);
        if(changeTemplateMapper.checkChangeTemplateNameIsRepeat(changeTemplate) > 0) {
            throw new ChangeTemplateNameRepeatException(name);
        }
        ChangeTemplateVo changeTemplateVo = changeTemplateMapper.getChangeTemplateById(changeTemplateId);
        if(changeTemplateVo == null) {
            throw new ChangeTemplateNotFoundException(changeTemplateId);
        }
        changeTemplateVo.setId(changeTemplate.getId());
        changeTemplateVo.setName(name);
        changeTemplateVo.setFcu(UserContext.get().getUserUuid(true));
        changeTemplateMapper.insertChangeTemplate(changeTemplateVo);
        List<Long> changeSopIdList = changeTemplateMapper.getChangeSopIdListByChangeTemplateId(changeTemplateId);
        if(CollectionUtils.isNotEmpty(changeSopIdList)) {
            int i = 0;
            for (Long changeSopId : changeSopIdList) {
                changeTemplateMapper.insertChangeTemplateSop(new ChangeTemplateSopVo(changeTemplate.getId(), changeSopId, i++));
            }
        }      
        return changeTemplate.getId();
    }

    public IValid name(){
        return value -> {
            ChangeTemplateVo changeTemplateVo = JSON.toJavaObject(value,ChangeTemplateVo.class);
            if (changeTemplateMapper.checkChangeTemplateNameIsRepeat(changeTemplateVo) > 0) {
                return new FieldValidResultVo(new ChangeTemplateNameRepeatException(changeTemplateVo.getName()));
            }
            return new FieldValidResultVo();
        };
    }

}

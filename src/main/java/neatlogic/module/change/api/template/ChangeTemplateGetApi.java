package neatlogic.module.change.api.template;

import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.process.auth.PROCESS_BASE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.Description;
import neatlogic.framework.restful.annotation.Input;
import neatlogic.framework.restful.annotation.OperationType;
import neatlogic.framework.restful.annotation.Output;
import neatlogic.framework.restful.annotation.Param;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.framework.change.dto.ChangeTemplateVo;
import neatlogic.module.change.service.ChangeTemplateService;
@Service
@AuthAction(action = PROCESS_BASE.class)
@OperationType(type = OperationTypeEnum.SEARCH)
public class ChangeTemplateGetApi extends PrivateApiComponentBase {
    
    @Autowired
    private ChangeTemplateService changeTemplateService;

    @Override
    public String getToken() {
        return "change/template/get";
    }

    @Override
    public String getName() {
        return "查询变更模板";
    }

    @Override
    public String getConfig() {
        return null;
    }
    
    @Input({
        @Param(name = "changeTemplateId", type = ApiParamType.LONG, isRequired = true, desc = "变更模板id")
    })
    @Output({
        @Param(name = "changeTemplate", explode = ChangeTemplateVo.class, desc = "变更模板")
    })
    @Description(desc = "查询变更模板")
    @Override
    public Object myDoService(JSONObject jsonObj) throws Exception {
        Long changeTemplateId = jsonObj.getLong("changeTemplateId");     
        return changeTemplateService.getChangeTemplateById(changeTemplateId);
    }

}

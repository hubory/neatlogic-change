package neatlogic.module.change.api.template;

import neatlogic.framework.asynchronization.threadlocal.UserContext;
import neatlogic.framework.auth.core.AuthAction;
import neatlogic.module.change.auth.label.CHANGE_MODIFY;
import neatlogic.framework.change.dto.ChangeTemplateSopVo;
import neatlogic.framework.change.dto.ChangeTemplateVo;
import neatlogic.framework.change.exception.ChangeTemplateNameRepeatException;
import neatlogic.framework.change.exception.ChangeTemplateNotFoundException;
import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.dto.FieldValidResultVo;
import neatlogic.framework.restful.annotation.*;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.core.IValid;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.framework.util.RegexUtils;
import neatlogic.module.change.dao.mapper.ChangeTemplateMapper;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service
@Transactional
@OperationType(type = OperationTypeEnum.CREATE)
@AuthAction(action = CHANGE_MODIFY.class)
public class ChangeTemplateSaveApi extends PrivateApiComponentBase {
    
    @Autowired
    private ChangeTemplateMapper changeTemplateMapper;

    @Override
    public String getToken() {
        return "change/template/save";
    }

    @Override
    public String getName() {
        return "保存变更模板";
    }

    @Override
    public String getConfig() {
        return null;
    }
    
    @Input({
        @Param(name = "id", type = ApiParamType.LONG, desc = "变更模板id"),
        @Param(name = "name", type = ApiParamType.REGEX, rule = RegexUtils.NAME, isRequired = true, maxLength = 50, desc = "名称"),
        @Param(name = "isActive", type = ApiParamType.ENUM, rule = "0,1", isRequired = true, desc = "是否激活(0,1)"),
        @Param(name = "authority", type = ApiParamType.STRING, desc = "分组uuid，格式：team#uuid"),
        @Param(name = "type", type = ApiParamType.STRING, desc = "类型"),
        @Param(name = "changeSopIdList", type = ApiParamType.JSONARRAY, desc = "变更sop模板id列表")
    })
    @Output({
        @Param(name = "id", type = ApiParamType.LONG, desc = "变更模板id")
    })
    @Description(desc = "保存变更模板")
    @Override
    public Object myDoService(JSONObject jsonObj) throws Exception {
        ChangeTemplateVo changeTemplateVo = JSON.toJavaObject(jsonObj, ChangeTemplateVo.class);
        if(changeTemplateMapper.checkChangeTemplateNameIsRepeat(changeTemplateVo) > 0) {
            throw new ChangeTemplateNameRepeatException(changeTemplateVo.getName());
        }
        Long id = jsonObj.getLong("id");
        if(id != null) {
            if(changeTemplateMapper.checkChangeTemplateIsExists(id) == 0) {
                throw new ChangeTemplateNotFoundException(id);
            }
            changeTemplateMapper.deleteChangeTemplateSopByChangeTemplateId(id);
            changeTemplateVo.setLcu(UserContext.get().getUserUuid(true));
            changeTemplateMapper.updateChangeTemplateById(changeTemplateVo);
        }else {
            changeTemplateVo.setFcu(UserContext.get().getUserUuid(true));
            changeTemplateMapper.insertChangeTemplate(changeTemplateVo);
        }
        int i = 0;
        for (Long changeSopId : changeTemplateVo.getChangeSopIdList()) {
            changeTemplateMapper.insertChangeTemplateSop(new ChangeTemplateSopVo(changeTemplateVo.getId(), changeSopId, i++));
        }
        return changeTemplateVo.getId();
    }

    public IValid name(){
        return value -> {
            ChangeTemplateVo changeTemplateVo = JSON.toJavaObject(value,ChangeTemplateVo.class);
            if (changeTemplateMapper.checkChangeTemplateNameIsRepeat(changeTemplateVo) > 0) {
                return new FieldValidResultVo(new ChangeTemplateNameRepeatException(changeTemplateVo.getName()));
            }
            return new FieldValidResultVo();
        };
    }

}

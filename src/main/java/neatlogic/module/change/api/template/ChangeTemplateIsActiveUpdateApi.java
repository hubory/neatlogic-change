package neatlogic.module.change.api.template;

import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.Description;
import neatlogic.framework.restful.annotation.Input;
import neatlogic.framework.restful.annotation.OperationType;
import neatlogic.framework.restful.annotation.Param;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.module.change.auth.label.CHANGE_MODIFY;
import neatlogic.module.change.dao.mapper.ChangeTemplateMapper;
import neatlogic.framework.change.dto.ChangeTemplateVo;
import neatlogic.framework.change.exception.ChangeTemplateNotFoundException;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service
@Transactional
@OperationType(type = OperationTypeEnum.UPDATE)
@AuthAction(action = CHANGE_MODIFY.class)
public class ChangeTemplateIsActiveUpdateApi extends PrivateApiComponentBase {
    
    @Autowired
    private ChangeTemplateMapper changeTemplateMapper;

    @Override
    public String getToken() {
        return "change/template/isactive/update";
    }

    @Override
    public String getName() {
        return "更新变更模板状态";
    }

    @Override
    public String getConfig() {
        return null;
    }
    
    @Input({
        @Param(name = "id", type = ApiParamType.LONG, isRequired = true, desc = "变更模板id"),
        @Param(name = "isActive", type = ApiParamType.ENUM, rule = "0,1", isRequired = true, desc = "是否激活(0,1)")
    })
    @Description(desc = "更新变更模板状态")
    @Override
    public Object myDoService(JSONObject jsonObj) throws Exception {
        ChangeTemplateVo changeTemplateVo = JSON.toJavaObject(jsonObj, ChangeTemplateVo.class);
        if(changeTemplateMapper.checkChangeTemplateIsExists(changeTemplateVo.getId()) == 0) {
            throw new ChangeTemplateNotFoundException(changeTemplateVo.getId());
        }
        changeTemplateMapper.updateChangeTemplateIsActiveById(changeTemplateVo);
        return null;
    }

}

package neatlogic.module.change.api;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.process.auth.PROCESS_BASE;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.*;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.module.change.dao.mapper.ChangeTemplateMapper;
import neatlogic.framework.change.dto.ChangeParamVo;
import neatlogic.framework.change.dto.ChangeSopStepVo;
import neatlogic.framework.change.dto.ChangeSopVo;
import neatlogic.framework.change.dto.ChangeStepVo;
import neatlogic.framework.change.dto.ChangeTemplateVo;
import neatlogic.framework.change.exception.ChangeSopNotFoundException;
import neatlogic.framework.change.exception.ChangeTemplateNotFoundException;
import neatlogic.module.change.service.ChangeTemplateService;

@Service
@AuthAction(action = PROCESS_BASE.class)
@OperationType(type = OperationTypeEnum.SEARCH)
public class ChangeStepListApi extends PrivateApiComponentBase {
    
    @Autowired
    private ChangeTemplateMapper changeTemplateMapper;
    
    @Autowired
    private ChangeTemplateService changeTemplateService;

    @Override
    public String getToken() {
        return "change/step/list";
    }

    @Override
    public String getName() {
        return "查询变更步骤列表";
    }

    @Override
    public String getConfig() {
        return null;
    }

    @Input({
        @Param(name = "changeTemplateId", type = ApiParamType.LONG, isRequired = true, desc = "变更模板id")
    })
    @Output({
        @Param(name = "changeStepList", explode = ChangeStepVo[].class, desc = "变更步骤列表"),
        @Param(name = "changeParamList", explode = ChangeParamVo[].class, desc = "变更变量列表")
    })
    @Description(desc = "查询变更步骤列表")
    @Override
    public Object myDoService(JSONObject jsonObj) throws Exception {
        Long changeTemplateId = jsonObj.getLong("changeTemplateId");
        ChangeTemplateVo changeTemplateVo = changeTemplateMapper.getChangeTemplateById(changeTemplateId);
        if(changeTemplateVo == null) {
            throw new ChangeTemplateNotFoundException(changeTemplateId);
        }

        JSONObject resultObj = new JSONObject();
        List<Long> changeSopIdList = changeTemplateMapper.getChangeSopIdListByChangeTemplateId(changeTemplateId);
        if(CollectionUtils.isNotEmpty(changeSopIdList)) {
            Set<Long> changeParamIdSet = new HashSet<>();
            List<ChangeStepVo> changeStepList = new ArrayList<>();
            int codeFirstNumberIncrement = 0;
            int codeFirstNumber = 0;
            for(Long changeSopId : changeSopIdList) {
                ChangeSopVo changeSopVo = changeTemplateMapper.getChangeSopById(changeSopId);
                if (changeSopVo == null) {
                    throw new ChangeSopNotFoundException(changeSopId);
                }
                /** 变更步骤列表 **/
                List<ChangeSopStepVo> changeSopStepList = changeTemplateMapper.getChangeSopStepListByChangeSopId(changeSopId);
                if (CollectionUtils.isNotEmpty(changeSopStepList)) {
                    List<Long> changeParamIdList = changeTemplateMapper.getChangeParamIdListByChangeSopId(changeSopId);
                    changeParamIdSet.addAll(changeParamIdList);
                    codeFirstNumberIncrement = codeFirstNumber;
                    changeSopStepList.sort((e1, e2) -> e1.getCode().compareTo(e2.getCode()));
                    /** 遍历变更步骤列表，填充描述内容、附件列表、处理人或组、回复列表 **/
                    for (ChangeSopStepVo changeSopStepVo : changeSopStepList) {
                        changeTemplateService.getChangeSopStepDetail(changeSopStepVo);
                        codeFirstNumber = updateCodeFirstNumber(changeSopStepVo, codeFirstNumberIncrement);
                        changeStepList.add(new ChangeStepVo(changeSopStepVo));
                    }
                }
            }
            changeStepList.sort((e1, e2) -> e1.getCode().compareToIgnoreCase(e2.getCode()));
            resultObj.put("changeStepList", changeStepList);
            if(CollectionUtils.isNotEmpty(changeParamIdSet)) {
                List<ChangeParamVo> changeParamList = changeTemplateMapper.getChangeParamListByIdSet(changeParamIdSet);
                resultObj.put("changeParamList", changeParamList);
            }
        }
        return resultObj;
    }

    private int updateCodeFirstNumber(ChangeSopStepVo changeSopStepVo, int codeFirstNumberIncrement) {
        int result = 0;
        if(changeSopStepVo.getCode().contains(".")) {
            String[] split = changeSopStepVo.getCode().split("\\.", 2);
            result = Integer.parseInt(split[0]);
            if(codeFirstNumberIncrement > 0) {
                result += codeFirstNumberIncrement;
                changeSopStepVo.setCode(result + "." + split[1]);
            }
        }else {
            result = Integer.parseInt(changeSopStepVo.getCode());
            if(codeFirstNumberIncrement > 0) {
                result += codeFirstNumberIncrement;
                changeSopStepVo.setCode(String.valueOf(result));
            }
        }
        return result;
    }
}

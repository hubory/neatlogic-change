package neatlogic.module.change.api;

import neatlogic.framework.asynchronization.threadlocal.UserContext;
import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.common.dto.ValueTextVo;
import neatlogic.framework.process.auth.PROCESS_BASE;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.*;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.framework.change.constvalue.ChangeOperationType;
import neatlogic.framework.change.dto.ChangeStepVo;
import neatlogic.framework.change.dto.ChangeVo;
import neatlogic.module.change.service.ChangeService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@AuthAction(action = PROCESS_BASE.class)
@Service
@OperationType(type = OperationTypeEnum.SEARCH)
public class ChangeGetApi extends PrivateApiComponentBase {

	@Autowired
	private ChangeService changeService;

	@Override
	public String getToken() {
		return "change/get";
	}

	@Override
	public String getName() {
		return "查询变更信息";
	}

	@Override
	public String getConfig() {
		return null;
	}
	@Input({
		@Param(name = "changeId", type = ApiParamType.LONG, isRequired = true, desc = "变更id")
	})
	@Output({
		@Param(name = "change", explode = ChangeVo.class, desc = "变更信息")
	})
	@Description(desc = "查询变更步骤信息")
	@Override
	public Object myDoService(JSONObject jsonObj) throws Exception {
		Long changeId = jsonObj.getLong("changeId"); 
		ChangeVo changeVo = changeService.getChangeById(changeId);
		if(changeService.isStartableChange(changeVo)) {
			changeVo.getActionList().add(new ValueTextVo(ChangeOperationType.STARTCHANGE.getValue(), ChangeOperationType.STARTCHANGE.getText()));
		}
		if(changeService.isCompletableChange(changeVo, UserContext.get().getUserUuid(true))) {
			changeVo.getActionList().add(new ValueTextVo(ChangeOperationType.SUCCEEDCHANGE.getValue(), ChangeOperationType.SUCCEEDCHANGE.getText()));
			changeVo.getActionList().add(new ValueTextVo(ChangeOperationType.FAILEDCHANGE.getValue(), ChangeOperationType.FAILEDCHANGE.getText()));
		}
		if(changeService.isRecoverableChange(changeVo)) {
			changeVo.getActionList().add(new ValueTextVo(ChangeOperationType.RECOVERCHANGE.getValue(), ChangeOperationType.RECOVERCHANGE.getText()));
		}
		if(changeService.isPauseableChange(changeVo)) {
			changeVo.getActionList().add(new ValueTextVo(ChangeOperationType.PAUSECHANGE.getValue(), ChangeOperationType.PAUSECHANGE.getText()));
		}
		if(changeService.isRestartableChange(changeVo)) {
			changeVo.getActionList().add(new ValueTextVo(ChangeOperationType.RESTARTCHANGE.getValue(), ChangeOperationType.RESTARTCHANGE.getText()));
		}
		if(changeService.isBatchUpdateChangeStepWorker(changeVo)) {
			changeVo.getActionList().add(new ValueTextVo(ChangeOperationType.BATCHEDITCHANGESTEPWORKER.getValue(), ChangeOperationType.BATCHEDITCHANGESTEPWORKER.getText()));
		}
		for(ChangeStepVo changeStepVo : changeVo.getChangeStepList()) {
			if(changeService.isAbortableChangeStep(changeVo, changeStepVo)) {
				changeStepVo.getActionList().add(new ValueTextVo(ChangeOperationType.ABORTCHANGESTEP.getValue(), ChangeOperationType.ABORTCHANGESTEP.getText()));
			}
			if(changeService.isStartableChangeStep(changeVo, changeStepVo)) {
				changeStepVo.getActionList().add(new ValueTextVo(ChangeOperationType.STARTCHANGESTEP.getValue(), ChangeOperationType.STARTCHANGESTEP.getText()));
			}
			if(changeService.isCompletableChangeStep(changeVo, changeStepVo)) {
				changeStepVo.getActionList().add(new ValueTextVo(ChangeOperationType.COMPLETECHANGESTEP.getValue(), ChangeOperationType.COMPLETECHANGESTEP.getText()));
			}
			if(changeService.isCommentableChangeStep(changeVo, changeStepVo)) {
				changeStepVo.getActionList().add(new ValueTextVo(ChangeOperationType.COMMENTCHANGESTEP.getValue(), ChangeOperationType.COMMENTCHANGESTEP.getText()));
			}
			if(changeService.isEditableChangeStep(changeVo, changeStepVo)) {
				changeStepVo.getActionList().add(new ValueTextVo(ChangeOperationType.EDITCHANGESTEP.getValue(), ChangeOperationType.EDITCHANGESTEP.getText()));
			}
		}
		return changeVo;
	}

}

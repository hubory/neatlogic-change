package neatlogic.module.change.api.sop;

import java.util.ArrayList;
import java.util.List;

import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.process.auth.PROCESS_BASE;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.common.dto.BasePageVo;
import neatlogic.framework.common.util.PageUtil;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.Description;
import neatlogic.framework.restful.annotation.Input;
import neatlogic.framework.restful.annotation.OperationType;
import neatlogic.framework.restful.annotation.Output;
import neatlogic.framework.restful.annotation.Param;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.module.change.dao.mapper.ChangeTemplateMapper;
import neatlogic.framework.change.dto.ChangeSopStepVo;
import neatlogic.framework.change.dto.ChangeSopVo;
import neatlogic.framework.change.dto.ChangeTemplateVo;
import neatlogic.module.change.service.ChangeTemplateService;
@Service
@AuthAction(action = PROCESS_BASE.class)
@OperationType(type = OperationTypeEnum.SEARCH)
public class ChangeSopListApi extends PrivateApiComponentBase {
    
    @Autowired
    private ChangeTemplateMapper changeTemplateMapper;
    
    @Autowired
    private ChangeTemplateService changeTemplateService;

    @Override
    public String getToken() {
        return "change/sop/list";
    }

    @Override
    public String getName() {
        return "查询变更sop模板列表";
    }

    @Override
    public String getConfig() {
        return null;
    }
    
    @Input({
        @Param(name = "keyword", type = ApiParamType.STRING, desc = "关键字，模糊匹配"),
        @Param(name = "authority", type = ApiParamType.STRING, desc = "分组uuid，格式：team#uuid"),
        @Param(name = "type", type = ApiParamType.STRING, desc = "变更sop模板类型"),
        @Param(name = "isActive", type = ApiParamType.ENUM, rule = "0,1", desc = "是否激活"),
        @Param(name = "needPage", type = ApiParamType.BOOLEAN, desc = "是否分页"),
        @Param(name = "currentPage", type = ApiParamType.INTEGER, desc = "当前页数"),
        @Param(name = "pageSize", type = ApiParamType.INTEGER, desc = "每页条数")
    })
    @Output({
        @Param(name = "changeSopList", explode = ChangeSopVo[].class, desc = "变更sop模板列表"),
        @Param(explode = BasePageVo.class)
    })
    @Description(desc = "查询变更sop模板列表")
    @Override
    public Object myDoService(JSONObject jsonObj) throws Exception {
        JSONObject resultObj = new JSONObject();
        resultObj.put("changeSopList", new ArrayList<>());
        int pageCount = 0;
        ChangeSopVo changeSopVo = JSON.toJavaObject(jsonObj, ChangeSopVo.class);
        if(changeSopVo.getNeedPage()) {
            int rowNum = changeTemplateMapper.getChangeSopCount(changeSopVo);
            pageCount = PageUtil.getPageCount(rowNum, changeSopVo.getPageSize());
            resultObj.put("currentPage", changeSopVo.getCurrentPage());
            resultObj.put("pageSize", changeSopVo.getPageSize());
            resultObj.put("pageCount", pageCount);
            resultObj.put("rowNum", rowNum);
        }
        
        if(!changeSopVo.getNeedPage() || changeSopVo.getCurrentPage() <= pageCount) {
            List<ChangeSopVo> changeSopList = changeTemplateMapper.getChangeSopList(changeSopVo);
            for(ChangeSopVo changeSop : changeSopList) {
                /** 变更步骤列表 **/
                List<ChangeSopStepVo> changeSopStepList = changeTemplateMapper.getChangeSopStepListByChangeSopId(changeSop.getId());
                if (CollectionUtils.isNotEmpty(changeSopStepList)) {
                    changeSopStepList.sort((e1, e2) -> e1.getCode().compareTo(e2.getCode()));
                    /** 遍历变更步骤列表，填充描述内容、附件列表、处理人或组、回复列表 **/
                    for (ChangeSopStepVo changeSopStepVo : changeSopStepList) {
                        changeTemplateService.getChangeSopStepDetail(changeSopStepVo);
                    }
                    changeSop.setChangeSopStepList(changeSopStepList);
                }
                /** 变更模板列表 **/
                List<Long> changeTemplateIdList = changeTemplateMapper.getChangeTemplateIdListByChangeSopId(changeSop.getId());
                if(CollectionUtils.isNotEmpty(changeTemplateIdList)) {
                    List<ChangeTemplateVo> changeTemplateList = changeTemplateMapper.getChangeTemplateListByIdList(changeTemplateIdList);
                    changeSop.setChangeTemplateList(changeTemplateList);
                }
            }
            resultObj.put("changeSopList", changeSopList);
        }       
        return resultObj;
    }

}

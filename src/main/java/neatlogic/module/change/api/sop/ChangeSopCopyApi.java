package neatlogic.module.change.api.sop;

import neatlogic.framework.asynchronization.threadlocal.UserContext;
import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.change.dto.*;
import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.common.constvalue.GroupSearch;
import neatlogic.framework.dto.FieldValidResultVo;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.*;
import neatlogic.framework.restful.core.IValid;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.framework.util.UuidUtil;
import neatlogic.module.change.auth.label.CHANGE_MODIFY;
import neatlogic.module.change.dao.mapper.ChangeMapper;
import neatlogic.module.change.dao.mapper.ChangeTemplateMapper;
import neatlogic.framework.change.exception.ChangeSopNameRepeatException;
import neatlogic.module.change.service.ChangeTemplateService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service
@Transactional
@OperationType(type = OperationTypeEnum.CREATE)
@AuthAction(action = CHANGE_MODIFY.class)
public class ChangeSopCopyApi extends PrivateApiComponentBase {
    
    @Autowired
    private ChangeTemplateMapper changeTemplateMapper;
    
    @Autowired
    private ChangeMapper changeMapper;
    
    @Autowired
    private ChangeTemplateService changeTemplateService;

    @Override
    public String getToken() {
        return "change/sop/copy";
    }

    @Override
    public String getName() {
        return "复制变更sop模板";
    }

    @Override
    public String getConfig() {
        return null;
    }
    
    @Input({
        @Param(name = "changeSopId", type = ApiParamType.LONG, isRequired = true, desc = "变更sop模板id"),
        @Param(name = "name", type = ApiParamType.STRING, isRequired = true, desc = "名称")
    })
    @Output({
        @Param(name = "id", type = ApiParamType.LONG, desc = "变更sop模板id")
    })
    @Description(desc = "复制变更sop模板")
    @Override
    public Object myDoService(JSONObject jsonObj) throws Exception {
        Long changeSopId = jsonObj.getLong("changeSopId");
        String name = jsonObj.getString("name");
        ChangeSopVo changeSop = new ChangeSopVo();
        changeSop.setName(name);
        if(changeTemplateMapper.checkChangeSopNameIsRepeat(changeSop) > 0) {
            throw new ChangeSopNameRepeatException(name);
        }
        ChangeSopVo changeSopVo = changeTemplateService.getChangeSopById(changeSopId);
        changeSopVo.setId(changeSop.getId());
        changeSopVo.setName(name);
        changeSopVo.setFcu(UserContext.get().getUserUuid(true));
        changeTemplateMapper.insertChangeSop(changeSopVo);
        for (ChangeSopStepVo changeSopStepVo : changeSopVo.getChangeSopStepList()) {
            changeSopStepVo.setUuid(UuidUtil.randomUuid());
            /** 保存步骤信息 **/
            changeSopStepVo.setChangeSopId(changeSopVo.getId());
            changeTemplateMapper.insertChangeSopStep(changeSopStepVo);
            /** 保存描述内容 **/
            if (StringUtils.isNotBlank(changeSopStepVo.getContent())) {
                ChangeContentVo contentVo = new ChangeContentVo(changeSopStepVo.getContent());
                changeMapper.replaceChangeContent(contentVo);
                changeTemplateMapper.insertChangeSopStepContent(new ChangeSopStepContentVo(changeSopVo.getId(), changeSopStepVo.getUuid(),
                    contentVo.getHash()));
            }
            /** 保存附件 **/
            if (CollectionUtils.isNotEmpty(changeSopStepVo.getFileIdList())) {
                for (Long fileId : changeSopStepVo.getFileIdList()) {
                    changeTemplateMapper.insertChangeSopStepFile(new ChangeSopStepFileVo(changeSopVo.getId(), changeSopStepVo.getUuid(),
                        fileId));
                }
            }
            /** 保存处理人或组 **/
            String worker = changeSopStepVo.getWorker();
            if (StringUtils.isNotBlank(worker) && worker.contains("#")) {
                String[] split = worker.split("#");
                if (GroupSearch.USER.getValue().equals(split[0])) {
                    changeTemplateMapper.insertChangeSopStepUser(
                        new ChangeSopStepUserVo(changeSopVo.getId(), changeSopStepVo.getUuid(), split[1]));
                } else if (GroupSearch.TEAM.getValue().equals(split[0])) {
                    changeTemplateMapper.insertChangeSopStepTeam(
                        new ChangeSopStepTeamVo(changeSopVo.getId(), changeSopStepVo.getUuid(), split[1]));
                }
            }
        }
        return changeSop.getId();
    }

    public IValid name(){
        return value -> {
            ChangeSopVo changeSopVo = JSON.toJavaObject(value,ChangeSopVo.class);
            if (changeTemplateMapper.checkChangeSopNameIsRepeat(changeSopVo) > 0) {
                return new FieldValidResultVo(new ChangeSopNameRepeatException(changeSopVo.getName()));
            }
            return new FieldValidResultVo();
        };
    }

}

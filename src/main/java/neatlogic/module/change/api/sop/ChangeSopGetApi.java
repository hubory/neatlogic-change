package neatlogic.module.change.api.sop;

import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.process.auth.PROCESS_BASE;
import neatlogic.module.change.dao.mapper.ChangeTemplateMapper;
import neatlogic.framework.change.dto.ChangeTemplateVo;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.Description;
import neatlogic.framework.restful.annotation.Input;
import neatlogic.framework.restful.annotation.OperationType;
import neatlogic.framework.restful.annotation.Output;
import neatlogic.framework.restful.annotation.Param;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.framework.change.dto.ChangeSopVo;
import neatlogic.module.change.service.ChangeTemplateService;

import javax.annotation.Resource;
import java.util.List;

@Service
@AuthAction(action = PROCESS_BASE.class)
@OperationType(type = OperationTypeEnum.SEARCH)
public class ChangeSopGetApi extends PrivateApiComponentBase {
    
    @Resource
    private ChangeTemplateService changeTemplateService;
    @Resource
    private ChangeTemplateMapper changeTemplateMapper;

    @Override
    public String getToken() {
        return "change/sop/get";
    }

    @Override
    public String getName() {
        return "查询变更sop模板";
    }

    @Override
    public String getConfig() {
        return null;
    }
    
    @Input({
        @Param(name = "changeSopId", type = ApiParamType.LONG, isRequired = true, desc = "变更sop模板id")
    })
    @Output({
        @Param(name = "changeSop", explode = ChangeSopVo.class, desc = "变更sop模板")
    })
    @Description(desc = "查询变更sop模板")
    @Override
    public Object myDoService(JSONObject jsonObj) throws Exception {
        Long changeSopId = jsonObj.getLong("changeSopId");
        ChangeSopVo changeSop = changeTemplateService.getChangeSopById(changeSopId);
        /** 变更模板列表 **/
        List<Long> changeTemplateIdList = changeTemplateMapper.getChangeTemplateIdListByChangeSopId(changeSop.getId());
        if(CollectionUtils.isNotEmpty(changeTemplateIdList)) {
            List<ChangeTemplateVo> changeTemplateList = changeTemplateMapper.getChangeTemplateListByIdList(changeTemplateIdList);
            changeSop.setChangeTemplateList(changeTemplateList);
        }
        return changeSop;
    }

}

/*
 * Copyright(c) 2023 NeatLogic Co., Ltd. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package neatlogic.module.change.api;

import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.change.constvalue.*;
import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.common.constvalue.GroupSearch;
import neatlogic.framework.process.auth.PROCESS_BASE;
import neatlogic.framework.process.dao.mapper.ProcessTaskMapper;
import neatlogic.framework.process.dto.ProcessTaskStepRemindVo;
import neatlogic.framework.process.dto.ProcessTaskStepVo;
import neatlogic.framework.process.exception.process.ProcessStepUtilHandlerNotFoundException;
import neatlogic.framework.process.stephandler.core.IProcessStepHandlerUtil;
import neatlogic.framework.process.stephandler.core.IProcessStepInternalHandler;
import neatlogic.framework.process.stephandler.core.ProcessStepInternalHandlerFactory;
import neatlogic.framework.restful.annotation.Description;
import neatlogic.framework.restful.annotation.Input;
import neatlogic.framework.restful.annotation.OperationType;
import neatlogic.framework.restful.annotation.Param;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.module.change.dao.mapper.ChangeMapper;
import neatlogic.framework.change.dto.ChangeStepVo;
import neatlogic.framework.change.dto.ChangeVo;
import neatlogic.framework.change.dto.ProcessTaskStepChangeVo;
import neatlogic.framework.change.exception.ChangeHandleHasNotStartedException;
import neatlogic.framework.change.exception.ChangeNoPermissionException;
import neatlogic.framework.change.exception.ChangeNotFoundException;
import neatlogic.module.change.service.ChangeService;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service
@Transactional
@AuthAction(action = PROCESS_BASE.class)
@OperationType(type = OperationTypeEnum.OPERATE)
public class ChangeRestartApi extends PrivateApiComponentBase {

	@Autowired
	private ChangeMapper changeMapper;
	
	@Autowired
	private ChangeService changeService;
    
    @Autowired
    private ProcessTaskMapper processTaskMapper;

    @Autowired
    private IProcessStepHandlerUtil IProcessStepHandlerUtil;

	@Override
	public String getToken() {
		return "change/restart";
	}

	@Override
	public String getName() {
		return "重新开始变更";
	}

	@Override
	public String getConfig() {
		return null;
	}

	@Input({
		@Param(name = "changeId", type = ApiParamType.LONG, isRequired = true, desc = "变更id"),
		@Param(name = "content", type = ApiParamType.STRING, desc = "描述"),
			@Param(name = "source", type = ApiParamType.STRING, defaultValue = "pc", desc = "来源")
	})
	@Description(desc = "重新开始变更")
	@Override
	public Object myDoService(JSONObject jsonObj) throws Exception {
		Long changeId = jsonObj.getLong("changeId");
		ChangeVo changeVo = changeMapper.getChangeById(changeId);
		if(changeVo == null) {
			throw new ChangeNotFoundException(changeId);
		}
		/* 获取当前变更锁 **/
		changeMapper.getChangeLockById(changeId);
		
		String owner = changeMapper.getChangeUserByChangeId(changeId);
		if(StringUtils.isNotBlank(owner)) {
			changeVo.setOwner(GroupSearch.USER.getValuePlugin() + owner);
		}
		changeVo.setChangeStepList(changeMapper.getChangeStepListByChangeId(changeId));
		if(!changeService.isRestartableChange(changeVo)) {
			throw new ChangeNoPermissionException(ChangeOperationType.RESTARTCHANGE.getText());
		}
		/** 重新开始变更时，将变更状态设置为“待处理”，变更步骤状态设置为“待处理”,is_active为0 **/
		changeVo.setStatus(ChangeStatus.PENDING.getValue());
		changeMapper.updateChangeStatus(changeVo);

		ChangeStepVo changeStepVo = new ChangeStepVo();
		changeStepVo.setAutoGenerateId(false);
		changeStepVo.setChangeId(changeId);
		changeStepVo.setStatus(ChangeStatus.PENDING.getValue());
		changeStepVo.setIsActive(0);
		changeMapper.updateChangeStepStatus(changeStepVo);
		changeMapper.updateChangeStepIsActive(changeStepVo);
		/** 将变更状态设置为“处理中” **/
		changeVo.setStatus(ChangeStatus.RUNNING.getValue());
		changeMapper.updateChangeStatus(changeVo);

		/** 重新开始变更时，触发激活变更步骤 **/
		changeService.activeChangeStep(changeId);

		/** 重新开始变更时，触发更新processtask_step_worker和processtask_step_user数据 **/
		ProcessTaskStepChangeVo processTaskStepChangeVo = changeMapper.getProcessTaskStepChangeHandleByChangeId(changeId);
		if (processTaskStepChangeVo == null) {
			throw new ChangeHandleHasNotStartedException();
		}
		IProcessStepInternalHandler processStepUtilHandler = ProcessStepInternalHandlerFactory.getHandler(ChangeProcessStepHandlerType.CHANGEHANDLE.getHandler());
		if (processStepUtilHandler == null) {
			throw new ProcessStepUtilHandlerNotFoundException(ChangeProcessStepHandlerType.CHANGEHANDLE.getHandler());
		}
		processStepUtilHandler.updateProcessTaskStepUserAndWorker(processTaskStepChangeVo.getProcessTaskId(), processTaskStepChangeVo.getProcessTaskStepId());

		/** 生成活动 **/
		ProcessTaskStepVo currentProcessTaskStepVo = processTaskMapper.getProcessTaskStepBaseInfoById(processTaskStepChangeVo.getProcessTaskStepId());
		currentProcessTaskStepVo.getParamObj().putAll(jsonObj);
		IProcessStepHandlerUtil.audit(currentProcessTaskStepVo, ChangeAuditType.RESTARTCHANGE);
		IProcessStepHandlerUtil.notify(currentProcessTaskStepVo, ChangeTriggerType.RESTARTCHANGE);
		IProcessStepHandlerUtil.action(currentProcessTaskStepVo, ChangeTriggerType.RESTARTCHANGE);
		processTaskMapper.deleteProcessTaskStepRemind(new ProcessTaskStepRemindVo(processTaskStepChangeVo.getProcessTaskStepId(), ChangeRemindType.PAUSECHANGE.getValue()));
		return null;
	}

}

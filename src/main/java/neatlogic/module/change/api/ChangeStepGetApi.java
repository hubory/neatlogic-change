package neatlogic.module.change.api;

import java.util.HashMap;
import java.util.Map;

import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.process.auth.PROCESS_BASE;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.common.constvalue.GroupSearch;
import neatlogic.framework.common.dto.ValueTextVo;
import neatlogic.framework.process.stephandler.core.ProcessStepInternalHandlerFactory;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.framework.change.constvalue.ChangeOperationType;
import neatlogic.module.change.dao.mapper.ChangeMapper;
import neatlogic.framework.change.dto.ChangeStepVo;
import neatlogic.framework.change.dto.ChangeVo;
import neatlogic.framework.change.dto.ProcessTaskStepChangeVo;
import neatlogic.framework.change.exception.ChangeStepNotFoundException;
import neatlogic.module.change.service.ChangeService;
@Service
@AuthAction(action = PROCESS_BASE.class)
@OperationType(type = OperationTypeEnum.SEARCH)
public class ChangeStepGetApi extends PrivateApiComponentBase {

	@Autowired
	private ChangeMapper changeMapper;
	
	@Autowired
	private ChangeService changeService;

	@Override
	public String getToken() {
		return "change/step/get";
	}

	@Override
	public String getName() {
		return "查询变更步骤信息";
	}

	@Override
	public String getConfig() {
		return null;
	}
	@Input({
		@Param(name = "changeStepId", type = ApiParamType.LONG, isRequired = true, desc = "变更步骤id")
	})
	@Output({
		@Param(name = "changeStep", explode = ChangeStepVo.class, desc = "变更步骤信息")
	})
	@Description(desc = "查询变更步骤信息")
	@Override
	public Object myDoService(JSONObject jsonObj) throws Exception {
		Long changeStepId = jsonObj.getLong("changeStepId"); 
		ChangeStepVo changeStepVo = changeMapper.getChangeStepById(changeStepId);
		if(changeStepVo == null) {
			throw new ChangeStepNotFoundException(changeStepId);
		}
		changeService.getChangeStepDetail(changeStepVo);
		ChangeVo changeVo = changeMapper.getChangeById(changeStepVo.getChangeId());
		String owner = changeMapper.getChangeUserByChangeId(changeStepVo.getChangeId());
		if(StringUtils.isNotBlank(owner)) {
			changeVo.setOwner(GroupSearch.USER.getValuePlugin() + owner);
		}
		Map<String, String> customButtonMap = new HashMap<>();
		ProcessTaskStepChangeVo processTaskStepChangeVo = changeMapper.getProcessTaskStepChangeHandleByChangeId(changeStepVo.getChangeId());
		if(processTaskStepChangeVo != null) {
			customButtonMap = ProcessStepInternalHandlerFactory.getHandler().getCustomButtonMapByProcessTaskStepId(processTaskStepChangeVo.getProcessTaskStepId());
		}
		if(changeService.isAbortableChangeStep(changeVo, changeStepVo)) {
		    String text = customButtonMap.get(ChangeOperationType.ABORTCHANGESTEP.getValue());
            if(StringUtils.isBlank(text)) {
                text = ChangeOperationType.ABORTCHANGESTEP.getText();
            }
			changeStepVo.getActionList().add(new ValueTextVo(ChangeOperationType.ABORTCHANGESTEP.getValue(), text));
		}
		if(changeService.isStartableChangeStep(changeVo, changeStepVo)) {
		    String text = customButtonMap.get(ChangeOperationType.STARTCHANGESTEP.getValue());
            if(StringUtils.isBlank(text)) {
                text = ChangeOperationType.STARTCHANGESTEP.getText();
            }
			changeStepVo.getActionList().add(new ValueTextVo(ChangeOperationType.STARTCHANGESTEP.getValue(), text));
		}
		if(changeService.isCompletableChangeStep(changeVo, changeStepVo)) {
		    String text = customButtonMap.get(ChangeOperationType.COMPLETECHANGESTEP.getValue());
            if(StringUtils.isBlank(text)) {
                text = ChangeOperationType.COMPLETECHANGESTEP.getText();
            }
			changeStepVo.getActionList().add(new ValueTextVo(ChangeOperationType.COMPLETECHANGESTEP.getValue(), text));
		}
		if(changeService.isCommentableChangeStep(changeVo, changeStepVo)) {
            String text = customButtonMap.get(ChangeOperationType.COMMENTCHANGESTEP.getValue());
            if(StringUtils.isBlank(text)) {
                text = ChangeOperationType.COMMENTCHANGESTEP.getText();
            }
			changeStepVo.getActionList().add(new ValueTextVo(ChangeOperationType.COMMENTCHANGESTEP.getValue(), text));
		}
		if(changeService.isEditableChangeStep(changeVo, changeStepVo)) {
            String text = customButtonMap.get(ChangeOperationType.EDITCHANGESTEP.getValue());
            if(StringUtils.isBlank(text)) {
                text = ChangeOperationType.EDITCHANGESTEP.getText();
            }
			changeStepVo.getActionList().add(new ValueTextVo(ChangeOperationType.EDITCHANGESTEP.getValue(), text));
		}
		return changeStepVo;
	}

}

package neatlogic.module.change.api.param;

import java.util.ArrayList;
import java.util.List;

import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.process.auth.PROCESS_BASE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.common.dto.BasePageVo;
import neatlogic.framework.common.util.PageUtil;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.Description;
import neatlogic.framework.restful.annotation.Input;
import neatlogic.framework.restful.annotation.OperationType;
import neatlogic.framework.restful.annotation.Output;
import neatlogic.framework.restful.annotation.Param;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.module.change.dao.mapper.ChangeTemplateMapper;
import neatlogic.framework.change.dto.ChangeParamVo;
@Service
@AuthAction(action = PROCESS_BASE.class)
@OperationType(type = OperationTypeEnum.SEARCH)
public class ChangeParamListApi extends PrivateApiComponentBase {
    
    @Autowired
    private ChangeTemplateMapper changeTemplateMapper;

    @Override
    public String getToken() {
        return "change/param/list";
    }

    @Override
    public String getName() {
        return "查询变更变量列表";
    }

    @Override
    public String getConfig() {
        return null;
    }
    
    @Input({
        @Param(name = "keyword", type = ApiParamType.STRING, desc = "关键字，模糊匹配"),
        @Param(name = "needPage", type = ApiParamType.BOOLEAN, desc = "是否分页"),
        @Param(name = "currentPage", type = ApiParamType.INTEGER, desc = "当前页数"),
        @Param(name = "pageSize", type = ApiParamType.INTEGER, desc = "每页条数")
    })
    @Output({
        @Param(name = "changeParamList", explode = ChangeParamVo[].class, desc = "变量列表"),
        @Param(explode = BasePageVo.class)
    })
    @Description(desc = "查询变更变量列表")
    @Override
    public Object myDoService(JSONObject jsonObj) throws Exception {
        JSONObject resultObj = new JSONObject();
        resultObj.put("changeParamList", new ArrayList<>());
        int pageCount = 0;
        BasePageVo basePageVo = JSON.toJavaObject(jsonObj, BasePageVo.class);
        if(basePageVo.getNeedPage()) {
            int rowNum = changeTemplateMapper.getChangeParamCount(basePageVo);
            pageCount = PageUtil.getPageCount(rowNum, basePageVo.getPageSize());
            resultObj.put("currentPage", basePageVo.getCurrentPage());
            resultObj.put("pageSize", basePageVo.getPageSize());
            resultObj.put("pageCount", pageCount);
            resultObj.put("rowNum", rowNum);
        }
        
        if(!basePageVo.getNeedPage() || basePageVo.getCurrentPage() <= pageCount) {
            List<ChangeParamVo> changeParamList = changeTemplateMapper.getChangeParamList(basePageVo);
            resultObj.put("changeParamList", changeParamList);
        }       
        return resultObj;
    }

}

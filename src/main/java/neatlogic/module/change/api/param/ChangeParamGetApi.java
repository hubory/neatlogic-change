package neatlogic.module.change.api.param;

import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.process.auth.PROCESS_BASE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.Description;
import neatlogic.framework.restful.annotation.Input;
import neatlogic.framework.restful.annotation.OperationType;
import neatlogic.framework.restful.annotation.Output;
import neatlogic.framework.restful.annotation.Param;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.module.change.dao.mapper.ChangeTemplateMapper;
import neatlogic.framework.change.dto.ChangeParamVo;
import neatlogic.framework.change.exception.ChangeParamNotFoundException;
@Service
@AuthAction(action = PROCESS_BASE.class)
@OperationType(type = OperationTypeEnum.SEARCH)
public class ChangeParamGetApi extends PrivateApiComponentBase {
    
    @Autowired
    private ChangeTemplateMapper changeTemplateMapper;
    
    @Override
    public String getToken() {
        return "change/param/get";
    }

    @Override
    public String getName() {
        return "查询变更变量";
    }

    @Override
    public String getConfig() {
        return null;
    }

    @Input({
        @Param(name = "changeParamId", type = ApiParamType.LONG, isRequired = true, desc = "变量id")
    })
    @Output({
        @Param(name = "changeParam", explode = ChangeParamVo.class, desc = "变量信息")
    })
    @Description(desc = "查询变更变量")
    @Override
    public Object myDoService(JSONObject jsonObj) throws Exception {
        Long changeParamId = jsonObj.getLong("changeParamId");
        ChangeParamVo changeParamVo = changeTemplateMapper.getChangeParamById(changeParamId);
        if(changeParamVo == null) {
            throw new ChangeParamNotFoundException(changeParamId.toString());
        }
        return changeParamVo;
    }

}

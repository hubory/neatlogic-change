package neatlogic.module.change.api.param;

import java.util.ArrayList;
import java.util.List;

import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.process.auth.PROCESS_BASE;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.Description;
import neatlogic.framework.restful.annotation.Input;
import neatlogic.framework.restful.annotation.OperationType;
import neatlogic.framework.restful.annotation.Output;
import neatlogic.framework.restful.annotation.Param;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.module.change.dao.mapper.ChangeTemplateMapper;
import neatlogic.framework.change.dto.ChangeSopStepVo;
import neatlogic.framework.change.exception.ChangeParamNotFoundException;
@Service
@AuthAction(action = PROCESS_BASE.class)
@OperationType(type = OperationTypeEnum.SEARCH)
public class ChangeParamReferenceListApi extends PrivateApiComponentBase {
    
    @Autowired
    private ChangeTemplateMapper changeTemplateMapper;
    
    @Override
    public String getToken() {
        return "change/param/reference/list";
    }

    @Override
    public String getName() {
        return "查询变更变量被引用列表";
    }

    @Override
    public String getConfig() {
        return null;
    }

    @Input({
        @Param(name = "changeParamId", type = ApiParamType.LONG, isRequired = true, desc = "变量id")
    })
    @Output({
        @Param(name = "changeSopStepList", explode = ChangeSopStepVo[].class, desc = "引用列表")
    })
    @Description(desc = "查询变更变量被引用列表")
    @Override
    public Object myDoService(JSONObject jsonObj) throws Exception {
        Long changeParamId = jsonObj.getLong("changeParamId");
        if(changeTemplateMapper.checkChangeParamIsExists(changeParamId) == 0) {
            throw new ChangeParamNotFoundException(changeParamId.toString());
        }
        List<String> changeSopStepUuidList = changeTemplateMapper.getChangeSopStepUuidListByChangeParamId(changeParamId);
        if(CollectionUtils.isNotEmpty(changeSopStepUuidList)) {
            List<ChangeSopStepVo> changeSopStepList = changeTemplateMapper.getChangeSopStepListByUuidList(changeSopStepUuidList);
            return changeSopStepList;
        }
        return new ArrayList<>();
    }

}

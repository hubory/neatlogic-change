package neatlogic.module.change.api.param;

import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.Description;
import neatlogic.framework.restful.annotation.Input;
import neatlogic.framework.restful.annotation.OperationType;
import neatlogic.framework.restful.annotation.Param;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.module.change.auth.label.CHANGE_MODIFY;
import neatlogic.module.change.dao.mapper.ChangeTemplateMapper;
import neatlogic.framework.change.dto.ChangeParamVo;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
@OperationType(type = OperationTypeEnum.DELETE)
@AuthAction(action = CHANGE_MODIFY.class)
public class ChangeParamDeleteApi extends PrivateApiComponentBase {
    
    @Autowired
    private ChangeTemplateMapper changeTemplateMapper;
    
    @Override
    public String getToken() {
        return "change/param/delete";
    }

    @Override
    public String getName() {
        return "删除变更变量";
    }

    @Override
    public String getConfig() {
        return null;
    }

    @Input({
        @Param(name = "changeParamId", type = ApiParamType.LONG, isRequired = true, desc = "变量id")
    })
    @Description(desc = "删除变更变量接口")
    @Override
    public Object myDoService(JSONObject jsonObj) throws Exception {
        Long changeParamId = jsonObj.getLong("changeParamId");
        List<String> changeSopStepUuidList = changeTemplateMapper.getChangeSopStepUuidListByChangeParamId(changeParamId);
        if(CollectionUtils.isNotEmpty(changeSopStepUuidList)) {
            ChangeParamVo changeParam = changeTemplateMapper.getChangeParamById(changeParamId);
            if(changeParam != null) {
                List<String> changeSopStepContentHashList = changeTemplateMapper.getChangeSopStepContentHashListByUuidList(changeSopStepUuidList);
                changeTemplateMapper.updateChangeSopStepContentByHashList(changeSopStepContentHashList, changeParam.getFreemarkerTemplate(), "");
            }
        }
        changeTemplateMapper.deleteChangeParamById(changeParamId);
        changeTemplateMapper.deleteChangeSopStepParamByChangeParamId(changeParamId);
        return null;
    }

}

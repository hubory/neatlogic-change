package neatlogic.module.change.api.param;

import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.dto.FieldValidResultVo;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.Description;
import neatlogic.framework.restful.annotation.Input;
import neatlogic.framework.restful.annotation.OperationType;
import neatlogic.framework.restful.annotation.Param;
import neatlogic.framework.restful.core.IValid;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.module.change.auth.label.CHANGE_MODIFY;
import neatlogic.module.change.dao.mapper.ChangeTemplateMapper;
import neatlogic.framework.change.dto.ChangeParamVo;
import neatlogic.framework.change.exception.ChangeParamNameRepeatException;
import neatlogic.framework.change.exception.ChangeParamNotFoundException;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
@OperationType(type = OperationTypeEnum.CREATE)
@AuthAction(action = CHANGE_MODIFY.class)
public class ChangeParamSaveApi extends PrivateApiComponentBase {
    
    @Autowired
    private ChangeTemplateMapper changeTemplateMapper;

    @Override
    public String getToken() {
        return "change/param/save";
    }

    @Override
    public String getName() {
        return "保存变更变量";
    }

    @Override
    public String getConfig() {
        return null;
    }

    @Input({
        @Param(name = "id", type = ApiParamType.LONG, desc = "变量id"),
        @Param(name = "name", type = ApiParamType.STRING, isRequired = true, desc = "变量名"),
        @Param(name = "mappingMethod", type = ApiParamType.ENUM, rule = "manualinput", isRequired = true, desc = "映射方式")
    })
    @Description(desc = "保存变更变量")
    @Override
    public Object myDoService(JSONObject jsonObj) throws Exception {
        ChangeParamVo changeParamVo = JSON.toJavaObject(jsonObj, ChangeParamVo.class);
        if(changeTemplateMapper.checkChangeParamNameIsRepeat(changeParamVo) > 0) {
            throw new ChangeParamNameRepeatException(changeParamVo.getName());
        }
        Long id = jsonObj.getLong("id");
        if(id != null) {
            ChangeParamVo changeParam = changeTemplateMapper.getChangeParamById(id);
            if(changeParam == null) {
                throw new ChangeParamNotFoundException(id.toString());
            }
            changeTemplateMapper.updateChangeParamById(changeParamVo);
            /** 如果修改了变量名，扫描sop步骤描述，替换变量名 **/
            if(!changeParam.getName().equals(changeParamVo.getName())) {
                List<String> changeSopStepUuidList = changeTemplateMapper.getChangeSopStepUuidListByChangeParamId(id);
                if(CollectionUtils.isNotEmpty(changeSopStepUuidList)) {
                   List<String> changeSopStepContentHashList = changeTemplateMapper.getChangeSopStepContentHashListByUuidList(changeSopStepUuidList);
                   changeTemplateMapper.updateChangeSopStepContentByHashList(changeSopStepContentHashList, changeParam.getFreemarkerTemplate(), changeParamVo.getFreemarkerTemplate());
                }
            }
        }else {
            changeTemplateMapper.insertChangeParam(changeParamVo);
        }
        return changeParamVo.getId();
    }

    public IValid name() {
        return value -> {
            ChangeParamVo changeParamVo = JSON.toJavaObject(value, ChangeParamVo.class);
            if (changeTemplateMapper.checkChangeParamNameIsRepeat(changeParamVo) > 0) {
                return new FieldValidResultVo(new ChangeParamNameRepeatException(changeParamVo.getName()));
            }
            return new FieldValidResultVo();
        };
    }

}

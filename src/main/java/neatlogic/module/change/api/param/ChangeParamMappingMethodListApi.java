package neatlogic.module.change.api.param;

import java.util.ArrayList;
import java.util.List;

import neatlogic.framework.auth.core.AuthAction;
import neatlogic.framework.process.auth.PROCESS_BASE;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import neatlogic.framework.common.dto.ValueTextVo;
import neatlogic.framework.restful.constvalue.OperationTypeEnum;
import neatlogic.framework.restful.annotation.Description;
import neatlogic.framework.restful.annotation.OperationType;
import neatlogic.framework.restful.annotation.Output;
import neatlogic.framework.restful.annotation.Param;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.framework.change.constvalue.ChangeParamMappingMethod;
@Deprecated
@Service
@AuthAction(action = PROCESS_BASE.class)
@OperationType(type = OperationTypeEnum.SEARCH)
public class ChangeParamMappingMethodListApi extends PrivateApiComponentBase {

    @Override
    public String getToken() {
        return "change/param/mappingmethod/list";
    }

    @Override
    public String getName() {
        return "查询变更变量映射方式列表";
    }

    @Override
    public String getConfig() {
        return null;
    }
    
    @Output({
        @Param(name = "mappingMethodList", explode = ValueTextVo[].class, desc = "映射方式列表")
    })
    @Description(desc = "查询变更变量映射方式列表")
    @Override
    public Object myDoService(JSONObject jsonObj) throws Exception {
        List<ValueTextVo> mappingMethodList = new ArrayList<>();
        for(ChangeParamMappingMethod type : ChangeParamMappingMethod.values()) {
            mappingMethodList.add(new ValueTextVo(type.getValue(), type.getText()));
        }
        return mappingMethodList;
    }

}

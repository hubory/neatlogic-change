package neatlogic.module.change.api;

import neatlogic.framework.process.constvalue.ProcessTaskSource;
import neatlogic.framework.process.stephandler.core.IProcessStepHandlerUtil;
import neatlogic.framework.change.constvalue.ChangeProcessStepHandlerType;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;

import neatlogic.framework.asynchronization.threadlocal.UserContext;
import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.process.dto.ProcessTaskStepVo;
import neatlogic.framework.process.exception.process.ProcessStepUtilHandlerNotFoundException;
import neatlogic.framework.process.stephandler.core.IProcessStepInternalHandler;
import neatlogic.framework.process.stephandler.core.ProcessStepInternalHandlerFactory;
import neatlogic.framework.restful.annotation.Description;
import neatlogic.framework.restful.annotation.Input;
import neatlogic.framework.restful.annotation.Param;
import neatlogic.framework.restful.core.privateapi.PrivateApiComponentBase;
import neatlogic.framework.change.constvalue.ChangeAuditType;
import neatlogic.framework.change.constvalue.ChangeOperationType;
import neatlogic.framework.change.constvalue.ChangeStatus;
import neatlogic.module.change.dao.mapper.ChangeMapper;
import neatlogic.framework.change.dto.ChangeVo;
import neatlogic.framework.change.dto.ProcessTaskStepChangeVo;
import neatlogic.framework.change.exception.ChangeHandleHasNotStartedException;
import neatlogic.framework.change.exception.ChangeNoPermissionException;
import neatlogic.framework.change.exception.ChangeNotFoundException;
import neatlogic.module.change.service.ChangeService;
//@Service
//@Transactional
public class ChangeCompleteApi extends PrivateApiComponentBase {

	@Autowired
	private ChangeMapper changeMapper;
	
	@Autowired
	private ChangeService changeService;

	@Autowired
	private IProcessStepHandlerUtil IProcessStepHandlerUtil;

	@Override
	public String getToken() {
		return "change/complete";
	}

	@Override
	public String getName() {
		return "完成变更";
	}

	@Override
	public String getConfig() {
		return null;
	}

	@Input({
		@Param(name = "changeId", type = ApiParamType.LONG, isRequired = true, desc = "变更id"),
		@Param(name = "action", type = ApiParamType.ENUM, rule = "succeedchange,failedchange", isRequired = true, desc = "操作类型（确认成功或确认失败）"),
		@Param(name = "content", type = ApiParamType.STRING, desc = "描述"),
		@Param(name = "source", type = ApiParamType.STRING, defaultValue = "pc", desc = "来源")
	})
	@Description(desc = "完成变更")
	@Override
	public Object myDoService(JSONObject jsonObj) throws Exception {
		Long changeId = jsonObj.getLong("changeId");
		if(changeMapper.checkChangeIsExists(changeId) == 0) {
			throw new ChangeNotFoundException(changeId);
		}
		/** 获取当前变更锁 **/
		changeMapper.getChangeLockById(changeId);
		String action = jsonObj.getString("action");
		ChangeVo changeVo = changeService.getChangeById(changeId);
		if(!changeService.isCompletableChange(changeVo, UserContext.get().getUserUuid(true))) {
			throw new ChangeNoPermissionException(ChangeOperationType.getText(action));
		}
		/** 完成变更时，将变更状态设置为“已成功”或“已失败” **/
		if(ChangeOperationType.SUCCEEDCHANGE.getValue().equals(action)) {
			changeVo.setStatus(ChangeStatus.SUCCEED.getValue());
		} else {
			changeVo.setStatus(ChangeStatus.FAILED.getValue());
		}
		changeMapper.updateChangeStatus(changeVo);

		/** 完成变更时，触发更新processtask_step_worker和processtask_step_user数据 **/
		ProcessTaskStepChangeVo processTaskStepChangeVo = changeMapper.getProcessTaskStepChangeHandleByChangeId(changeId);
		if (processTaskStepChangeVo == null) {
			throw new ChangeHandleHasNotStartedException();
		}
		IProcessStepInternalHandler processStepUtilHandler = ProcessStepInternalHandlerFactory.getHandler(ChangeProcessStepHandlerType.CHANGEHANDLE.getHandler());
		if (processStepUtilHandler == null) {
			throw new ProcessStepUtilHandlerNotFoundException(ChangeProcessStepHandlerType.CHANGEHANDLE.getHandler());
		}
		processStepUtilHandler.updateProcessTaskStepUserAndWorker(processTaskStepChangeVo.getProcessTaskId(), processTaskStepChangeVo.getProcessTaskStepId());

		/** 生成活动 **/
		ProcessTaskStepVo currentProcessTaskStepVo = new ProcessTaskStepVo();
		currentProcessTaskStepVo.setProcessTaskId(processTaskStepChangeVo.getProcessTaskId());
		currentProcessTaskStepVo.setId(processTaskStepChangeVo.getProcessTaskStepId());
		currentProcessTaskStepVo.getParamObj().put("source", jsonObj.getString("source"));
		IProcessStepHandlerUtil.audit(currentProcessTaskStepVo, ChangeAuditType.getChangeAuditType(action));
		return null;
	}

}

package neatlogic.module.change.audithandler.handler;

import neatlogic.framework.process.audithandler.core.IProcessTaskStepAuditDetailHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.google.common.base.Objects;

import neatlogic.framework.process.dto.ProcessTaskStepAuditDetailVo;
import neatlogic.framework.change.constvalue.ChangeAuditDetailType;
import neatlogic.framework.change.dto.ChangeVo;
@Service
public class ChangeInfoAuditHandler implements IProcessTaskStepAuditDetailHandler {
	
	@Override
	public String getType() {
		return ChangeAuditDetailType.CHANGEINFO.getValue();
	}

	@Override
	public int handle(ProcessTaskStepAuditDetailVo processTaskStepAuditDetailVo) {
		if(StringUtils.isNotBlank(processTaskStepAuditDetailVo.getNewContent())) {
		    JSONObject planStartEndTime = null;
		    JSONObject timeWindow = null;
		    JSONObject autoStart = null;
		    JSONObject content = null;
		    JSONObject file = null;
		    
            JSONArray change = new JSONArray();
			ChangeVo changeVo = JSON.parseObject(processTaskStepAuditDetailVo.getNewContent(), new TypeReference<ChangeVo>(){});
			if(CollectionUtils.isNotEmpty(changeVo.getPlanStartEndTime())) {
			    planStartEndTime = new JSONObject();
	            planStartEndTime.put("type", "planStartEndTime");
	            planStartEndTime.put("typeName", "计划起止时间");
	            planStartEndTime.put("newContent", changeVo.getPlanStartEndTime());
				planStartEndTime.put("changeType", "new");
	            change.add(planStartEndTime);
			}
			
			if(changeVo.getStartTimeWindow() != null || changeVo.getEndTimeWindow() != null) {
	            timeWindow = new JSONObject();
	            timeWindow.put("type", "timeWindow");
	            timeWindow.put("typeName", "时间窗口");
	            timeWindow.put("newContent", changeVo.getStartTimeWindow() + "-" + changeVo.getEndTimeWindow());
				timeWindow.put("changeType", "new");
	            change.add(timeWindow);
			}
			
			if(changeVo.getAutoStart() != null) {
	            autoStart = new JSONObject();
	            autoStart.put("type", "autoStart");
	            autoStart.put("typeName", "自动开始");
	            autoStart.put("newContent", Objects.equal(changeVo.getAutoStart(), 1) ? "是" : "否");
				autoStart.put("changeType", "new");
	            change.add(autoStart);
			}
			
			if(changeVo.getContent() != null) {
			    content = new JSONObject();
	            content.put("type", "content");
	            content.put("typeName", "变更说明");
	            content.put("newContent", changeVo.getContent());
				content.put("changeType", "new");
	            change.add(content);
			}
			
			if(changeVo.getFileList() != null) {
			    file = new JSONObject();
	            file.put("type", "fileList");
	            file.put("typeName", "附件");
	            file.put("newContent", changeVo.getFileList());
				file.put("changeType", "new");
			}
			
			boolean isShowFile = false;
			if(StringUtils.isNotBlank(processTaskStepAuditDetailVo.getOldContent())) {
				ChangeVo oldChangeVo = JSON.parseObject(processTaskStepAuditDetailVo.getOldContent(), new TypeReference<ChangeVo>(){});		
				if(planStartEndTime != null && !Objects.equal(oldChangeVo.getPlanStartEndTime(), changeVo.getPlanStartEndTime())) {
					planStartEndTime.put("oldContent", oldChangeVo.getPlanStartEndTime());
					planStartEndTime.put("changeType", "update");
				}
				if(timeWindow != null) {
					if(!Objects.equal(oldChangeVo.getStartTimeWindow(), changeVo.getStartTimeWindow()) || !Objects.equal(oldChangeVo.getEndTimeWindow(), changeVo.getEndTimeWindow())) {
	                    timeWindow.put("oldContent", oldChangeVo.getStartTimeWindow() + "-" + oldChangeVo.getEndTimeWindow());
						timeWindow.put("changeType", "update");
					}
				}

				if(autoStart != null && !Objects.equal(oldChangeVo.getAutoStart(), changeVo.getAutoStart())) {
					autoStart.put("oldContent", Objects.equal(oldChangeVo.getAutoStart(), 1) ? "是" : "否");
					autoStart.put("changeType", "update");
				}
				
				if(content != null && !Objects.equal(oldChangeVo.getContent(), changeVo.getContent())) {
                    content.put("oldContent", oldChangeVo.getContent());
					content.put("changeType", "update");
                }

				if(file != null) {
				    if(!Objects.equal(oldChangeVo.getFileList(), changeVo.getFileList())) {
                        isShowFile = true;
	                    file.put("oldContent", oldChangeVo.getFileList());
						file.put("changeType", "update");
				    }else if(CollectionUtils.isNotEmpty(oldChangeVo.getFileList()) || CollectionUtils.isNotEmpty(changeVo.getFileList())) {
				        isShowFile = true;
				    }
				}
			}
			if(isShowFile) {
                change.add(file);
			}
			processTaskStepAuditDetailVo.setOldContent(null);
			processTaskStepAuditDetailVo.setNewContent(JSON.toJSONString(change));
		}
		return 1;
	}

}

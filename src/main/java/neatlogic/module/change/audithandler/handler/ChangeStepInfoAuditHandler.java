package neatlogic.module.change.audithandler.handler;

import neatlogic.framework.process.audithandler.core.IProcessTaskStepAuditDetailHandler;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.google.common.base.Objects;

import neatlogic.framework.common.constvalue.GroupSearch;
import neatlogic.framework.dao.mapper.TeamMapper;
import neatlogic.framework.dao.mapper.UserMapper;
import neatlogic.framework.dto.TeamVo;
import neatlogic.framework.dto.UserVo;
import neatlogic.framework.process.dto.ProcessTaskStepAuditDetailVo;
import neatlogic.framework.change.constvalue.ChangeAuditDetailType;
import neatlogic.framework.change.dto.ChangeStepVo;
@Service
public class ChangeStepInfoAuditHandler implements IProcessTaskStepAuditDetailHandler {
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private TeamMapper teamMapper;

	@Override
	public String getType() {
		return ChangeAuditDetailType.CHANGESTEPINFO.getValue();
	}

	@Override
	public int handle(ProcessTaskStepAuditDetailVo processTaskStepAuditDetailVo) {
		if(StringUtils.isNotBlank(processTaskStepAuditDetailVo.getNewContent())) {
			ChangeStepVo changeStepVo = JSON.parseObject(processTaskStepAuditDetailVo.getNewContent(), new TypeReference<ChangeStepVo>(){});
			JSONObject planStartDate = new JSONObject();
			planStartDate.put("type", "planStartDate");
			planStartDate.put("typeName", "计划开始日期");
			planStartDate.put("newContent", changeStepVo.getPlanStartDate());
			planStartDate.put("changeType", "new");
			
			JSONObject timeWindow = new JSONObject();
			timeWindow.put("type", "timeWindow");
			timeWindow.put("typeName", "时间窗口");
			timeWindow.put("newContent", changeStepVo.getStartTimeWindow() + "-" + changeStepVo.getEndTimeWindow());
			timeWindow.put("changeType", "new");

			JSONObject worker = new JSONObject();
			worker.put("type", "worker");
			worker.put("typeName", "处理人");
			worker.put("changeType", "new");
			if(changeStepVo.getWorker().startsWith(GroupSearch.USER.getValuePlugin())) {
				UserVo userVo = userMapper.getUserBaseInfoByUuid(changeStepVo.getWorker().substring(5));
				if(userVo != null) {
					JSONObject newWorker = new JSONObject();
					newWorker.put("initType", GroupSearch.USER.getValue());
					newWorker.put("uuid", userVo.getUuid());
					newWorker.put("name", userVo.getUserName());
					worker.put("newContent", newWorker);
				}
			}else {
				TeamVo teamVo = teamMapper.getTeamByUuid(changeStepVo.getWorker().substring(5));
				if(teamVo != null) {
					JSONObject newWorker = new JSONObject();
					newWorker.put("initType", GroupSearch.TEAM.getValue());
					newWorker.put("uuid", teamVo.getUuid());
					newWorker.put("name", teamVo.getName());
					worker.put("newContent", newWorker);
				}
			}

			if(StringUtils.isNotBlank(processTaskStepAuditDetailVo.getOldContent())) {
				ChangeStepVo oldChangeStepVo = JSON.parseObject(processTaskStepAuditDetailVo.getOldContent(), new TypeReference<ChangeStepVo>(){});		

				if(!Objects.equal(oldChangeStepVo.getPlanStartDate(), changeStepVo.getPlanStartDate())) {
					planStartDate.put("oldContent", oldChangeStepVo.getPlanStartDate());
					planStartDate.put("changeType", "update");
				}
				if(!Objects.equal(oldChangeStepVo.getStartTimeWindow(), changeStepVo.getStartTimeWindow()) || !Objects.equal(oldChangeStepVo.getEndTimeWindow(), changeStepVo.getEndTimeWindow())) {
					timeWindow.put("oldContent", oldChangeStepVo.getStartTimeWindow() + "-" + oldChangeStepVo.getEndTimeWindow());
					timeWindow.put("changeType", "update");
				}
				if(!Objects.equal(oldChangeStepVo.getWorker(), changeStepVo.getWorker())) {
					if(oldChangeStepVo.getWorker().startsWith(GroupSearch.USER.getValuePlugin())) {
						UserVo userVo = userMapper.getUserBaseInfoByUuid(oldChangeStepVo.getWorker().substring(5));
						if(userVo != null) {
							JSONObject oldWorker = new JSONObject();
							oldWorker.put("initType", GroupSearch.USER.getValue());
							oldWorker.put("uuid", userVo.getUuid());
							oldWorker.put("name", userVo.getUserName());
							worker.put("oldContent", oldWorker);
							worker.put("changeType", "update");
						}
					}else {
						TeamVo teamVo = teamMapper.getTeamByUuid(oldChangeStepVo.getWorker().substring(5));
						if(teamVo != null) {
							JSONObject oldWorker = new JSONObject();
							oldWorker.put("initType", GroupSearch.TEAM.getValue());
							oldWorker.put("uuid", teamVo.getUuid());
							oldWorker.put("name", teamVo.getName());
							worker.put("oldContent", oldWorker);
							worker.put("changeType", "update");
						}
					}
				}
			}

			JSONArray newContentObj = new JSONArray();
			newContentObj.add(planStartDate);
			newContentObj.add(timeWindow);
			newContentObj.add(worker);
			processTaskStepAuditDetailVo.setOldContent(null);
			processTaskStepAuditDetailVo.setNewContent(JSON.toJSONString(newContentObj));
		}
		return 1;
	}

}

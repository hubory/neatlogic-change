package neatlogic.module.change.stephandler.utilhandler;

import neatlogic.framework.asynchronization.threadlocal.UserContext;
import neatlogic.framework.change.dto.*;
import neatlogic.framework.common.constvalue.GroupSearch;
import neatlogic.framework.common.dto.ValueTextVo;
import neatlogic.framework.crossover.CrossoverServiceFactory;
import neatlogic.framework.dto.UserVo;
import neatlogic.framework.notify.crossover.INotifyServiceCrossoverService;
import neatlogic.framework.process.constvalue.ProcessTaskOperationType;
import neatlogic.framework.process.constvalue.ProcessTaskStepUserStatus;
import neatlogic.framework.process.constvalue.ProcessUserType;
import neatlogic.framework.process.dto.*;
import neatlogic.framework.process.dto.processconfig.ActionConfigActionVo;
import neatlogic.framework.process.dto.processconfig.ActionConfigVo;
import neatlogic.framework.notify.dto.InvokeNotifyPolicyConfigVo;
import neatlogic.framework.process.stephandler.core.ProcessStepInternalHandlerBase;
import neatlogic.framework.process.util.ProcessConfigUtil;
import neatlogic.framework.change.constvalue.ChangeOperationType;
import neatlogic.framework.change.constvalue.ChangeProcessStepHandlerType;
import neatlogic.framework.change.constvalue.ChangeStatus;
import neatlogic.module.change.dao.mapper.ChangeMapper;
import neatlogic.module.change.notify.handler.ChangeHandleNotifyPolicyHandler;
import neatlogic.module.change.service.ChangeService;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONPath;
import com.google.common.base.Objects;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ChangeHandleProcessUtilHandler extends ProcessStepInternalHandlerBase {

    @Autowired
    private ChangeMapper changeMapper;

    @Autowired
    private ChangeService changeService;

    @Override
    public String getHandler() {
        return ChangeProcessStepHandlerType.CHANGEHANDLE.getHandler();
    }

    @Override
    public Object getHandlerStepInfo(ProcessTaskStepVo currentProcessTaskStepVo) {
        return null;
    }

    @Override
    public Object getHandlerStepInitInfo(ProcessTaskStepVo currentProcessTaskStepVo) {
        Long changeId = changeMapper.getChangeIdByProcessTaskStepId(currentProcessTaskStepVo.getId());
        if (changeId != null && changeMapper.checkChangeIsExists(changeId) > 0) {
            Map<String, String> customButtonMap = getCustomButtonMapByProcessTaskStepId(currentProcessTaskStepVo.getId());
            ChangeVo changeVo = changeService.getChangeById(changeId);
            if (changeService.isStartableChange(changeVo)) {
                String text = customButtonMap.get(ChangeOperationType.STARTCHANGE.getValue());
                if (StringUtils.isBlank(text)) {
                    text = ChangeOperationType.STARTCHANGE.getText();
                }
                changeVo.getActionList().add(new ValueTextVo(ChangeOperationType.STARTCHANGE.getValue(), text));
            }
            if (changeService.isCompletableChange(changeVo, UserContext.get().getUserUuid(true))) {
                String text = customButtonMap.get(ChangeOperationType.SUCCEEDCHANGE.getValue());
                if (StringUtils.isBlank(text)) {
                    text = ChangeOperationType.SUCCEEDCHANGE.getText();
                }
                changeVo.getActionList().add(new ValueTextVo(ChangeOperationType.SUCCEEDCHANGE.getValue(), text));
                text = customButtonMap.get(ChangeOperationType.FAILEDCHANGE.getValue());
                if (StringUtils.isBlank(text)) {
                    text = ChangeOperationType.FAILEDCHANGE.getText();
                }
                changeVo.getActionList().add(new ValueTextVo(ChangeOperationType.FAILEDCHANGE.getValue(), text));
            }
            if (changeService.isRecoverableChange(changeVo)) {
                String text = customButtonMap.get(ChangeOperationType.RECOVERCHANGE.getValue());
                if (StringUtils.isBlank(text)) {
                    text = ChangeOperationType.RECOVERCHANGE.getText();
                }
                changeVo.getActionList().add(new ValueTextVo(ChangeOperationType.RECOVERCHANGE.getValue(), text));
            }
            if (changeService.isPauseableChange(changeVo)) {
                String text = customButtonMap.get(ChangeOperationType.PAUSECHANGE.getValue());
                if (StringUtils.isBlank(text)) {
                    text = ChangeOperationType.PAUSECHANGE.getText();
                }
                changeVo.getActionList().add(new ValueTextVo(ChangeOperationType.PAUSECHANGE.getValue(), text));
            }
            if (changeService.isRestartableChange(changeVo)) {
                String text = customButtonMap.get(ChangeOperationType.RESTARTCHANGE.getValue());
                if (StringUtils.isBlank(text)) {
                    text = ChangeOperationType.RESTARTCHANGE.getText();
                }
                changeVo.getActionList().add(new ValueTextVo(ChangeOperationType.RESTARTCHANGE.getValue(), text));
            }
            if (changeService.isBatchUpdateChangeStepWorker(changeVo)) {
                String text = customButtonMap.get(ChangeOperationType.BATCHEDITCHANGESTEPWORKER.getValue());
                if (StringUtils.isBlank(text)) {
                    text = ChangeOperationType.BATCHEDITCHANGESTEPWORKER.getText();
                }
                changeVo.getActionList().add(new ValueTextVo(ChangeOperationType.BATCHEDITCHANGESTEPWORKER.getValue(), text));
            }
            if (changeService.isEditableChange(changeVo)) {
                String text = customButtonMap.get(ChangeOperationType.EDITCHANGE.getValue());
                if (StringUtils.isBlank(text)) {
                    text = ChangeOperationType.EDITCHANGE.getText();
                }
                changeVo.getActionList().add(new ValueTextVo(ChangeOperationType.EDITCHANGE.getValue(), text));
            }
            for (ChangeStepVo changeStepVo : changeVo.getChangeStepList()) {
                if (changeService.isAbortableChangeStep(changeVo, changeStepVo)) {
                    String text = customButtonMap.get(ChangeOperationType.ABORTCHANGESTEP.getValue());
                    if (StringUtils.isBlank(text)) {
                        text = ChangeOperationType.ABORTCHANGESTEP.getText();
                    }
                    changeStepVo.getActionList().add(new ValueTextVo(ChangeOperationType.ABORTCHANGESTEP.getValue(), text));
                }
                if (changeService.isStartableChangeStep(changeVo, changeStepVo)) {
                    String text = customButtonMap.get(ChangeOperationType.STARTCHANGESTEP.getValue());
                    if (StringUtils.isBlank(text)) {
                        text = ChangeOperationType.STARTCHANGESTEP.getText();
                    }
                    changeStepVo.getActionList().add(new ValueTextVo(ChangeOperationType.STARTCHANGESTEP.getValue(), text));
                }
                if (changeService.isCompletableChangeStep(changeVo, changeStepVo)) {
                    String text = customButtonMap.get(ChangeOperationType.COMPLETECHANGESTEP.getValue());
                    if (StringUtils.isBlank(text)) {
                        text = ChangeOperationType.COMPLETECHANGESTEP.getText();
                    }
                    changeStepVo.getActionList().add(new ValueTextVo(ChangeOperationType.COMPLETECHANGESTEP.getValue(), text));
                }
                if (changeService.isCommentableChangeStep(changeVo, changeStepVo)) {
                    String text = customButtonMap.get(ChangeOperationType.COMMENTCHANGESTEP.getValue());
                    if (StringUtils.isBlank(text)) {
                        text = ChangeOperationType.COMMENTCHANGESTEP.getText();
                    }
                    changeStepVo.getActionList().add(new ValueTextVo(ChangeOperationType.COMMENTCHANGESTEP.getValue(), text));
                }
                if (changeService.isEditableChangeStep(changeVo, changeStepVo)) {
                    String text = customButtonMap.get(ChangeOperationType.EDITCHANGESTEP.getValue());
                    if (StringUtils.isBlank(text)) {
                        text = ChangeOperationType.EDITCHANGESTEP.getText();
                    }
                    changeStepVo.getActionList().add(new ValueTextVo(ChangeOperationType.EDITCHANGESTEP.getValue(), text));
                }
            }
            List<ChangeStatusVo> changeStatusVoList = new ArrayList<>();
            changeStatusVoList.add(new ChangeStatusVo(ChangeStatus.PENDING.getValue()));
            changeStatusVoList.add(new ChangeStatusVo(ChangeStatus.RUNNING.getValue()));
            changeStatusVoList.add(new ChangeStatusVo(ChangeStatus.SUCCEED.getValue()));
            changeStatusVoList.add(new ChangeStatusVo(ChangeStatus.ABORTED.getValue()));
            changeVo.setChangeStatusVoList(changeStatusVoList);
            return changeVo;
        }
        return null;
    }

    @Override
    public void makeupProcessStep(ProcessStepVo processStepVo, JSONObject stepConfigObj) {
        /** 组装通知策略id **/
        JSONObject notifyPolicyConfig = stepConfigObj.getJSONObject("notifyPolicyConfig");
        InvokeNotifyPolicyConfigVo invokeNotifyPolicyConfigVo = JSONObject.toJavaObject(notifyPolicyConfig, InvokeNotifyPolicyConfigVo.class);
        if (invokeNotifyPolicyConfigVo != null) {
            processStepVo.setNotifyPolicyConfig(invokeNotifyPolicyConfigVo);
        }

        JSONObject actionConfig = stepConfigObj.getJSONObject("actionConfig");
        ActionConfigVo actionConfigVo = JSONObject.toJavaObject(actionConfig, ActionConfigVo.class);
        if (actionConfigVo != null) {
            List<ActionConfigActionVo> actionList = actionConfigVo.getActionList();
            if (CollectionUtils.isNotEmpty(actionList)) {
                List<String> integrationUuidList = new ArrayList<>();
                for (ActionConfigActionVo actionVo : actionList) {
                    String integrationUuid = actionVo.getIntegrationUuid();
                    if (StringUtils.isNotBlank(integrationUuid)) {
                        integrationUuidList.add(integrationUuid);
                    }
                }
                processStepVo.setIntegrationUuidList(integrationUuidList);
            }
        }
        //保存回复模版ID
        Long commentTemplateId = stepConfigObj.getLong("commentTemplateId");
        processStepVo.setCommentTemplateId(commentTemplateId);

        JSONArray tagList = stepConfigObj.getJSONArray("tagList");
        if (CollectionUtils.isNotEmpty(tagList)) {
            processStepVo.setTagList(tagList.toJavaList(String.class));
        }

        //保存子任务
        JSONObject taskConfig = stepConfigObj.getJSONObject("taskConfig");
        if(MapUtils.isNotEmpty(taskConfig)){
            ProcessStepTaskConfigVo taskConfigVo = JSONObject.toJavaObject(taskConfig,ProcessStepTaskConfigVo.class);
            processStepVo.setTaskConfigVo(taskConfigVo);
        }
        // 保存表单场景
        String formSceneUuid = stepConfigObj.getString("formSceneUuid");
        if (StringUtils.isNotBlank(formSceneUuid)) {
            processStepVo.setFormSceneUuid(formSceneUuid);
        }
    }

    @Override
    public void updateProcessTaskStepUserAndWorker(Long processTaskId, Long processTaskStepId) {
        Long changeId = changeMapper.getChangeIdByProcessTaskStepId(processTaskStepId);
        if (changeId != null) {
            /** 查出change_step_user和change_step_team表中当前步骤的变更步骤处理人和组列表 **/
            Set<String> runningChangeStepTeamUuidSet = new HashSet<>();
            Set<String> runningChangeStepUserUuidSet = new HashSet<>();
            Set<String> succeedChangeStepUserUuidSet = new HashSet<>();
            ChangeVo changeVo = changeMapper.getChangeById(changeId);
            if (changeVo != null && ChangeStatus.RUNNING.getValue().equals(changeVo.getStatus())) {
                String whenChangeStepUserVisible = "processTaskStepActive";
                ProcessTaskStepVo processTaskStepVo = processTaskMapper.getProcessTaskStepBaseInfoById(processTaskStepId);
                String stepConfig = selectContentByHashMapper.getProcessTaskStepConfigByHash(processTaskStepVo.getConfigHash());
                if (StringUtils.isNotBlank(stepConfig)) {
                    whenChangeStepUserVisible = (String) JSONPath.read(stepConfig,"whenChangeStepUserVisible");
                }
                List<ChangeStepVo> changeStepList = changeMapper.getChangeStepListByChangeId(changeId);
                for (ChangeStepVo changeStepVo : changeStepList) {
                    if (Objects.equal(changeStepVo.getIsActive(), 1) || "processTaskStepActive".equals(whenChangeStepUserVisible)) {
                        ChangeStepUserVo changeStepUserVo = changeMapper.getChangeStepUserByChangeStepId(changeStepVo.getId());
                        if (ChangeStatus.PENDING.getValue().equals(changeStepVo.getStatus())) {
                            if (changeStepUserVo != null) {
                                runningChangeStepUserUuidSet.add(changeStepUserVo.getUserVo().getUuid());
                            } else {
                                ChangeStepTeamVo changeStepTeamVo = changeMapper.getChangeStepTeamByChangeStepId(changeStepVo.getId());
                                if (changeStepTeamVo != null) {
                                    runningChangeStepTeamUuidSet.add(changeStepTeamVo.getTeamUuid());
                                }
                            }
                        } else if (ChangeStatus.RUNNING.getValue().equals(changeStepVo.getStatus())) {
                            runningChangeStepUserUuidSet.add(changeStepUserVo.getUserVo().getUuid());
                        } else {
                            succeedChangeStepUserUuidSet.add(changeStepUserVo.getUserVo().getUuid());
                        }
                    }
                }
            }

            /** 查出processtask_step_worker表中当前步骤的变更步骤处理人和组列表 **/
            Set<String> workerMinorUserUuidSet = new HashSet<>();
            Set<String> workerMinorTeamUuidSet = new HashSet<>();
            List<ProcessTaskStepWorkerVo> workerList = processTaskMapper.getProcessTaskStepWorkerByProcessTaskIdAndProcessTaskStepId(processTaskId, processTaskStepId);
            for (ProcessTaskStepWorkerVo workerVo : workerList) {
                if (ProcessUserType.MINOR.getValue().equals(workerVo.getUserType())) {
                    if (GroupSearch.USER.getValue().equals(workerVo.getType())) {
                        workerMinorUserUuidSet.add(workerVo.getUuid());
                    } else if (GroupSearch.TEAM.getValue().equals(workerVo.getType())) {
                        workerMinorTeamUuidSet.add(workerVo.getUuid());
                    }
                }
            }

            /** 查出processtask_step_user表中当前步骤的变更步骤处理人列表 **/
            Set<String> doingMinorUserUuidSet = new HashSet<>();
            Set<String> doneMinorUserUuidSet = new HashSet<>();
            List<ProcessTaskStepUserVo> minorUserList = processTaskMapper.getProcessTaskStepUserByStepId(processTaskStepId, ProcessUserType.MINOR.getValue());
            for (ProcessTaskStepUserVo userVo : minorUserList) {
                if (ProcessTaskStepUserStatus.DOING.getValue().equals(userVo.getStatus())) {
                    doingMinorUserUuidSet.add(userVo.getUserUuid());
                } else if (ProcessTaskStepUserStatus.DONE.getValue().equals(userVo.getStatus())) {
                    doneMinorUserUuidSet.add(userVo.getUserUuid());
                }
            }

            ProcessTaskStepWorkerVo processTaskStepWorkerVo = new ProcessTaskStepWorkerVo();
            processTaskStepWorkerVo.setProcessTaskId(processTaskId);
            processTaskStepWorkerVo.setProcessTaskStepId(processTaskStepId);
            processTaskStepWorkerVo.setUserType(ProcessUserType.MINOR.getValue());

            ProcessTaskStepUserVo processTaskStepUserVo = new ProcessTaskStepUserVo();
            processTaskStepUserVo.setProcessTaskId(processTaskId);
            processTaskStepUserVo.setProcessTaskStepId(processTaskStepId);
            processTaskStepUserVo.setUserType(ProcessUserType.MINOR.getValue());
            /** 删除processtask_step_worker表中当前步骤多余的变更步骤处理人 **/
            List<String> needDeleteUserList = ListUtils.removeAll(workerMinorUserUuidSet, runningChangeStepUserUuidSet);
            for (String userUuid : needDeleteUserList) {
                processTaskStepWorkerVo.setUuid(userUuid);
                processTaskMapper.deleteProcessTaskStepWorker(processTaskStepWorkerVo);
                //System.out.println("deleteProcessTaskStepWorker" + userUuid);
                if (succeedChangeStepUserUuidSet.contains(userUuid)) {
                    if (doingMinorUserUuidSet.contains(userUuid)) {
                        /** 完成变更步骤 **/
                        processTaskStepUserVo.setUserUuid(userUuid);
                        processTaskStepUserVo.setStatus(ProcessTaskStepUserStatus.DONE.getValue());
                        processTaskMapper.updateProcessTaskStepUserStatus(processTaskStepUserVo);
                        //System.out.println("updateProcessTaskStepUserStatus" + userUuid);
                    }
                } else {
                    if (doingMinorUserUuidSet.contains(userUuid)) {
                        /** 取消变更步骤 **/
                        processTaskStepUserVo.setUserUuid(userUuid);
                        processTaskMapper.deleteProcessTaskStepUser(processTaskStepUserVo);
                        //System.out.println("deleteProcessTaskStepUser" + userUuid);
                    }
                }
            }
            /** 向processtask_step_worker表中插入当前步骤的变更步骤处理人 **/
            List<String> needInsertUserList = ListUtils.removeAll(runningChangeStepUserUuidSet, workerMinorUserUuidSet);
            processTaskStepWorkerVo.setType(GroupSearch.USER.getValue());
            for (String userUuid : needInsertUserList) {
                processTaskStepWorkerVo.setUuid(userUuid);
                processTaskMapper.insertIgnoreProcessTaskStepWorker(processTaskStepWorkerVo);
                //System.out.println("insertProcessTaskStepWorker" + userUuid);

                if (doneMinorUserUuidSet.contains(userUuid)) {
                    /** 重做变更步骤 **/
                    processTaskStepUserVo.setUserUuid(userUuid);
                    processTaskStepUserVo.setStatus(ProcessTaskStepUserStatus.DOING.getValue());
                    processTaskMapper.updateProcessTaskStepUserStatus(processTaskStepUserVo);
                    //System.out.println("updateProcessTaskStepUserStatus" + userUuid);
                } else if (!doingMinorUserUuidSet.contains(userUuid)) {
                    /** 开始变更步骤 **/
                    UserVo userVo = userMapper.getUserBaseInfoByUuid(userUuid);
                    if (userVo != null) {
                        processTaskStepUserVo.setUserUuid(userVo.getUuid());
                        processTaskStepUserVo.setUserName(userVo.getUserName());
                        processTaskStepUserVo.setStatus(ProcessTaskStepUserStatus.DOING.getValue());
                        processTaskMapper.insertProcessTaskStepUser(processTaskStepUserVo);
                        //System.out.println("insertProcessTaskStepUser" + userUuid);
                    }
                }
            }
            /** 删除processtask_step_worker表中当前步骤多余的变更步骤处理组 **/
            List<String> needDeleteTeamList = ListUtils.removeAll(workerMinorTeamUuidSet, runningChangeStepTeamUuidSet);
            for (String TeamUuid : needDeleteTeamList) {
                /** 开始变更步骤 **/
                processTaskStepWorkerVo.setUuid(TeamUuid);
                processTaskMapper.deleteProcessTaskStepWorker(processTaskStepWorkerVo);
                //System.out.println("deleteProcessTaskStepWorker" + TeamUuid);
            }
            /** 向processtask_step_worker表中插入当前步骤的变更步骤处理组 **/
            List<String> needInsertTeamList = ListUtils.removeAll(runningChangeStepTeamUuidSet, workerMinorTeamUuidSet);
            processTaskStepWorkerVo.setType(GroupSearch.TEAM.getValue());
            for (String TeamUuid : needInsertTeamList) {
                /** 激活变更步骤 **/
                processTaskStepWorkerVo.setUuid(TeamUuid);
                processTaskMapper.insertIgnoreProcessTaskStepWorker(processTaskStepWorkerVo);
                //System.out.println("insertProcessTaskStepWorker" + TeamUuid);
            }

            /** 变更处理回退到变更创建，重新激活变更创建，并删除某些变更步骤 **/
            if (doingMinorUserUuidSet.size() + doneMinorUserUuidSet.size() > runningChangeStepUserUuidSet.size() + succeedChangeStepUserUuidSet.size()) {
                List<String> minorUserUuidList = new ArrayList<>(doingMinorUserUuidSet.size() + doneMinorUserUuidSet.size());
                minorUserUuidList.addAll(doingMinorUserUuidSet);
                minorUserUuidList.addAll(doneMinorUserUuidSet);
                List<String> changeStepUserUuidList = new ArrayList<>(runningChangeStepUserUuidSet.size() + succeedChangeStepUserUuidSet.size());
                changeStepUserUuidList.addAll(runningChangeStepUserUuidSet);
                changeStepUserUuidList.addAll(succeedChangeStepUserUuidSet);
                List<String> needDeleteUserUuidList = ListUtils.removeAll(minorUserUuidList, changeStepUserUuidList);
                for (String userUuid : needDeleteUserUuidList) {
                    processTaskStepUserVo.setUserUuid(userUuid);
                    processTaskMapper.deleteProcessTaskStepUser(processTaskStepUserVo);
                }
            }
        }
    }

    @SuppressWarnings("serial")
    @Override
    public JSONObject makeupConfig(JSONObject configObj) {
        if (configObj == null) {
            configObj = new JSONObject();
        }
        JSONObject resultObj = new JSONObject();

        /** 授权 **/
        ProcessTaskOperationType[] stepActions = {
                ProcessTaskOperationType.STEP_VIEW,
                ProcessTaskOperationType.STEP_TRANSFER,
                ProcessTaskOperationType.STEP_RETREAT
        };
        JSONArray authorityList = configObj.getJSONArray("authorityList");
        JSONArray authorityArray = ProcessConfigUtil.regulateAuthorityList(authorityList, stepActions);
        resultObj.put("authorityList", authorityArray);

        /** 按钮映射列表 **/
        ProcessTaskOperationType[] stepButtons = {
                ProcessTaskOperationType.STEP_COMPLETE,
                ProcessTaskOperationType.STEP_BACK,
                ProcessTaskOperationType.STEP_COMMENT,
                ProcessTaskOperationType.PROCESSTASK_TRANSFER,
                ProcessTaskOperationType.STEP_ACCEPT,
                ProcessTaskOperationType.PROCESSTASK_ABORT,
                ProcessTaskOperationType.PROCESSTASK_RECOVER
        };
        /** 变更按钮映射列表 **/
        ChangeOperationType[] changeButtons = {
                ChangeOperationType.STARTCHANGE,
                ChangeOperationType.PAUSECHANGE,
                ChangeOperationType.RECOVERCHANGE,
                ChangeOperationType.RESTARTCHANGE,
                ChangeOperationType.SUCCEEDCHANGE,
                ChangeOperationType.FAILEDCHANGE,
                ChangeOperationType.EDITCHANGE,
                ChangeOperationType.BATCHEDITCHANGESTEPWORKER
        };
        /** 变更步骤按钮映射列表 **/
        ChangeOperationType[] changeStepButtons = {
                ChangeOperationType.STARTCHANGESTEP,
                ChangeOperationType.COMPLETECHANGESTEP,
                ChangeOperationType.ABORTCHANGESTEP,
                ChangeOperationType.COMMENTCHANGESTEP,
                ChangeOperationType.EDITCHANGESTEP
        };
        JSONArray customButtonList = configObj.getJSONArray("customButtonList");
        JSONArray customButtonArray = ProcessConfigUtil.regulateCustomButtonList(customButtonList, stepButtons);
        JSONArray changeCustomButtonArray = ProcessConfigUtil.regulateCustomButtonList(customButtonList, changeButtons, "变更");
        JSONArray changeStepCustomButtonArray = ProcessConfigUtil.regulateCustomButtonList(customButtonList, changeStepButtons, "变更步骤");
        customButtonArray.addAll(changeCustomButtonArray);
        customButtonArray.addAll(changeStepCustomButtonArray);
        resultObj.put("customButtonList", customButtonArray);
        /** 状态映射列表 **/
        JSONArray customStatusList = configObj.getJSONArray("customStatusList");
        JSONArray customStatusArray = ProcessConfigUtil.regulateCustomStatusList(customStatusList);
        resultObj.put("customStatusList", customStatusArray);

        /** 可替换文本列表 **/
        resultObj.put("replaceableTextList", ProcessConfigUtil.regulateReplaceableTextList(configObj.getJSONArray("replaceableTextList")));
        /* 任务 */
        JSONObject taskConfig = configObj.getJSONObject("taskConfig");
        resultObj.put("taskConfig",taskConfig);
        return resultObj;
    }

    @Override
    public JSONObject regulateProcessStepConfig(JSONObject configObj) {
        if (configObj == null) {
            configObj = new JSONObject();
        }
        JSONObject resultObj = new JSONObject();

        /** 授权 **/
        ProcessTaskOperationType[] stepActions = {
                ProcessTaskOperationType.STEP_VIEW,
                ProcessTaskOperationType.STEP_TRANSFER,
                ProcessTaskOperationType.STEP_RETREAT
        };
        JSONArray authorityList = null;
        Integer enableAuthority = configObj.getInteger("enableAuthority");
        if (java.util.Objects.equals(enableAuthority, 1)) {
            authorityList = configObj.getJSONArray("authorityList");
        } else {
            enableAuthority = 0;
        }
        resultObj.put("enableAuthority", enableAuthority);
        JSONArray authorityArray = ProcessConfigUtil.regulateAuthorityList(authorityList, stepActions);
        resultObj.put("authorityList", authorityArray);

        /** 通知 **/
        JSONObject notifyPolicyConfig = configObj.getJSONObject("notifyPolicyConfig");
        INotifyServiceCrossoverService notifyServiceCrossoverService = CrossoverServiceFactory.getApi(INotifyServiceCrossoverService.class);
        InvokeNotifyPolicyConfigVo invokeNotifyPolicyConfigVo = notifyServiceCrossoverService.regulateNotifyPolicyConfig(notifyPolicyConfig, ChangeHandleNotifyPolicyHandler.class);
        resultObj.put("notifyPolicyConfig", invokeNotifyPolicyConfigVo);

        /** 动作 **/
        JSONObject actionConfig = configObj.getJSONObject("actionConfig");
        ActionConfigVo actionConfigVo = JSONObject.toJavaObject(actionConfig, ActionConfigVo.class);
        if (actionConfigVo == null) {
            actionConfigVo = new ActionConfigVo();
        }
        actionConfigVo.setHandler(ChangeHandleNotifyPolicyHandler.class.getName());
        resultObj.put("actionConfig", actionConfigVo);

        /** 按钮映射列表 **/
        ProcessTaskOperationType[] stepButtons = {
                ProcessTaskOperationType.STEP_COMPLETE,
                ProcessTaskOperationType.STEP_BACK,
                ProcessTaskOperationType.STEP_COMMENT,
                ProcessTaskOperationType.PROCESSTASK_TRANSFER,
                ProcessTaskOperationType.STEP_ACCEPT,
                ProcessTaskOperationType.PROCESSTASK_ABORT,
                ProcessTaskOperationType.PROCESSTASK_RECOVER
        };

        /** 变更按钮映射列表 **/
        ChangeOperationType[] changeButtons = {
                ChangeOperationType.STARTCHANGE,
                ChangeOperationType.PAUSECHANGE,
                ChangeOperationType.RECOVERCHANGE,
                ChangeOperationType.RESTARTCHANGE,
                ChangeOperationType.SUCCEEDCHANGE,
                ChangeOperationType.FAILEDCHANGE,
                ChangeOperationType.EDITCHANGE,
                ChangeOperationType.BATCHEDITCHANGESTEPWORKER
        };
        /** 变更步骤按钮映射列表 **/
        ChangeOperationType[] changeStepButtons = {
                ChangeOperationType.STARTCHANGESTEP,
                ChangeOperationType.COMPLETECHANGESTEP,
                ChangeOperationType.ABORTCHANGESTEP,
                ChangeOperationType.COMMENTCHANGESTEP,
                ChangeOperationType.EDITCHANGESTEP
        };
        JSONArray customButtonList = configObj.getJSONArray("customButtonList");
        JSONArray customButtonArray = ProcessConfigUtil.regulateCustomButtonList(customButtonList, stepButtons);
        JSONArray changeCustomButtonArray = ProcessConfigUtil.regulateCustomButtonList(customButtonList, changeButtons, "变更");
        JSONArray changeStepCustomButtonArray = ProcessConfigUtil.regulateCustomButtonList(customButtonList, changeStepButtons, "变更步骤");
        customButtonArray.addAll(changeCustomButtonArray);
        customButtonArray.addAll(changeStepCustomButtonArray);
        resultObj.put("customButtonList", customButtonArray);
        /** 状态映射列表 **/
        JSONArray customStatusList = configObj.getJSONArray("customStatusList");
        JSONArray customStatusArray = ProcessConfigUtil.regulateCustomStatusList(customStatusList);
        resultObj.put("customStatusList", customStatusArray);

        /** 可替换文本列表 **/
        resultObj.put("replaceableTextList", ProcessConfigUtil.regulateReplaceableTextList(configObj.getJSONArray("replaceableTextList")));

        /** 回复模板 **/
        Long commentTemplateId = configObj.getLong("commentTemplateId");
        if (commentTemplateId != null) {
            resultObj.put("commentTemplateId", commentTemplateId);
        }

        /** 关联变更 **/
        String linkedChange = configObj.getString("linkedChange");
        if (linkedChange == null) {
            linkedChange = "";
        }
        resultObj.put("linkedChange", linkedChange);

        /** 变更步骤设为待办 **/
        String whenChangeStepUserVisible = configObj.getString("whenChangeStepUserVisible");
        if (whenChangeStepUserVisible == null) {
            whenChangeStepUserVisible = "changeStepActive";
        }
        resultObj.put("whenChangeStepUserVisible", whenChangeStepUserVisible);

        /* 任务 */
        JSONObject taskConfig = configObj.getJSONObject("taskConfig");
        resultObj.put("taskConfig",taskConfig);

        JSONArray tagList = configObj.getJSONArray("tagList");
        if (tagList == null) {
            tagList = new JSONArray();
        }
        resultObj.put("tagList", tagList);
        /** 表单场景 **/
        String formSceneUuid = configObj.getString("formSceneUuid");
        String formSceneName = configObj.getString("formSceneName");
        resultObj.put("formSceneUuid", formSceneUuid == null ? "" : formSceneUuid);
        resultObj.put("formSceneName", formSceneName == null ? "" : formSceneName);
        return resultObj;
    }

}

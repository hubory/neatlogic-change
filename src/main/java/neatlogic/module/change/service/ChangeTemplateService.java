package neatlogic.module.change.service;

import neatlogic.framework.change.dto.ChangeSopStepVo;
import neatlogic.framework.change.dto.ChangeSopVo;
import neatlogic.framework.change.dto.ChangeTemplateVo;

public interface ChangeTemplateService {

    public ChangeSopVo getChangeSopById(Long changeSopId);
    
    public ChangeSopStepVo getChangeSopStepDetail(ChangeSopStepVo changeSopStepVo);

    public ChangeTemplateVo getChangeTemplateById(Long changeTemplateId);
}

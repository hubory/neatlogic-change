/*
Copyright(c) 2023 NeatLogic Co., Ltd. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. 
 */

package neatlogic.module.change.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import neatlogic.framework.dto.TeamVo;
import neatlogic.framework.process.dto.ProcessTaskStepVo;
import neatlogic.framework.process.stephandler.core.IProcessStepHandlerUtil;
import neatlogic.framework.change.constvalue.ChangeAuditType;
import neatlogic.framework.change.constvalue.ChangeProcessStepHandlerType;
import neatlogic.framework.change.constvalue.ChangeTriggerType;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;

import neatlogic.framework.asynchronization.threadlocal.UserContext;
import neatlogic.framework.common.constvalue.GroupSearch;
import neatlogic.framework.dao.mapper.TeamMapper;
import neatlogic.framework.dao.mapper.UserMapper;
import neatlogic.framework.dto.UserVo;
import neatlogic.framework.file.dao.mapper.FileMapper;
import neatlogic.framework.file.dto.FileVo;
import neatlogic.framework.process.constvalue.ProcessTaskStatus;
import neatlogic.framework.process.dao.mapper.ProcessTaskMapper;
import neatlogic.framework.process.dto.ProcessTaskVo;
import neatlogic.framework.process.exception.process.ProcessStepHandlerNotFoundException;
import neatlogic.framework.process.stephandler.core.IProcessStepInternalHandler;
import neatlogic.framework.process.stephandler.core.ProcessStepInternalHandlerFactory;
import neatlogic.framework.util.TimeUtil;
import neatlogic.framework.change.constvalue.ChangeStatus;
import neatlogic.module.change.dao.mapper.ChangeMapper;
import neatlogic.framework.change.dto.ChangeStepCommentVo;
import neatlogic.framework.change.dto.ChangeStepTeamVo;
import neatlogic.framework.change.dto.ChangeStepUserVo;
import neatlogic.framework.change.dto.ChangeStepVo;
import neatlogic.framework.change.dto.ChangeVo;
import neatlogic.framework.change.dto.ProcessTaskStepChangeVo;
import neatlogic.framework.change.exception.ChangeHandleHasNotStartedException;
import neatlogic.framework.change.exception.ChangeNotFoundException;

/**
 * 
 * @Author:linbq
 * @Time:2020年8月17日
 * @ClassName: ChangeServiceImpl
 */
@Service
public class ChangeServiceImpl implements ChangeService {

    @Autowired
    private TeamMapper teamMapper;

    @Autowired
    private ChangeMapper changeMapper;

    @Autowired
    private FileMapper fileMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ProcessTaskMapper processTaskMapper;

    @Autowired
    private IProcessStepHandlerUtil IProcessStepHandlerUtil;

    @Override
    public boolean isStartableChangeStep(ChangeVo changeVo, ChangeStepVo changeStepVo) {
        /** 变更状态为“处理中”，变更步骤已激活，状态为“待处理”，变更步骤处理人或属于变更步骤处理组时才有“开始”操作权限 **/
        if (processTaskIsRunning(changeVo.getId()) && ChangeStatus.RUNNING.getValue().equals(changeVo.getStatus())
            && Objects.equals(changeStepVo.getIsActive(), 1) && changeStepVo.getIsAfterStartDate() == 1
            && changeStepVo.getIsInWindow() == 1) {
            if (ChangeStatus.PENDING.getValue().equals(changeStepVo.getStatus())) {
                return checkCurrentUserIsWorker(changeStepVo.getWorker());
            }
        }
        return false;
    }

    @Override
    public boolean isAbortableChangeStep(ChangeVo changeVo, ChangeStepVo changeStepVo) {
        /** 变更状态为“处理中”，变更步骤已激活，状态为“待处理”或“处理中”，变更步骤处理人或属于变更步骤处理组时才有“取消”操作权限 **/
        if (processTaskIsRunning(changeVo.getId()) && ChangeStatus.RUNNING.getValue().equals(changeVo.getStatus())
            && Objects.equals(changeStepVo.getIsActive(), 1) && changeStepVo.getIsAfterStartDate() == 1
            && changeStepVo.getIsInWindow() == 1) {
            if (ChangeStatus.PENDING.getValue().equals(changeStepVo.getStatus())
                || ChangeStatus.RUNNING.getValue().equals(changeStepVo.getStatus())) {
                return checkCurrentUserIsWorker(changeStepVo.getWorker());
            }
        }
        return false;
    }

    @Override
    public boolean isCompletableChangeStep(ChangeVo changeVo, ChangeStepVo changeStepVo) {
        /** 变更状态为“处理中”，变更步骤已激活，状态为“处理中”，变更步骤处理人或属于变更步骤处理组时才有“完成”操作权限 **/
        if (processTaskIsRunning(changeVo.getId()) && ChangeStatus.RUNNING.getValue().equals(changeVo.getStatus())
            && Objects.equals(changeStepVo.getIsActive(), 1) && changeStepVo.getIsInWindow() == 1) {
            if (ChangeStatus.RUNNING.getValue().equals(changeStepVo.getStatus())) {
                return checkCurrentUserIsWorker(changeStepVo.getWorker());
            }
        }
        return false;
    }

    @Override
    public boolean isCommentableChangeStep(ChangeVo changeVo, ChangeStepVo changeStepVo) {
        /** 变更状态为“处理中”，变更步骤已激活，状态不为“已取消”，变更步骤处理人或属于变更步骤处理组时才有“评论”操作权限 **/
        if (processTaskIsRunning(changeVo.getId()) && ChangeStatus.RUNNING.getValue().equals(changeVo.getStatus())
            && Objects.equals(changeStepVo.getIsActive(), 1) && changeStepVo.getIsAfterStartDate() == 1
            && changeStepVo.getIsInWindow() == 1) {
            if (!ChangeStatus.ABORTED.getValue().equals(changeStepVo.getStatus())) {
                return checkCurrentUserIsWorker(changeStepVo.getWorker());
            }
        }
        return false;
    }

    @Override
    public boolean isEditableChangeStep(ChangeVo changeVo, ChangeStepVo changeStepVo) {
        /** 变更状态为“待处理”或“已暂停”，变更经理时才有“编辑”操作权限 **/
        if (processTaskIsRunning(changeVo.getId()) && ChangeStatus.PENDING.getValue().equals(changeVo.getStatus())
            || ChangeStatus.PAUSED.getValue().equals(changeVo.getStatus())) {
            if (ChangeStatus.PENDING.getValue().equals(changeStepVo.getStatus())
                || ChangeStatus.RUNNING.getValue().equals(changeStepVo.getStatus())) {
                if (UserContext.get().getUserUuid(true).equals(changeVo.getOwner().substring(GroupSearch.USER.getValuePlugin().length()))) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean isBatchUpdateChangeStepWorker(ChangeVo changeVo) {
        /** 变更状态为“待处理”时，变更经理才有“批量编辑处理人”操作权限 **/
        if (processTaskIsRunning(changeVo.getId()) && ChangeStatus.PENDING.getValue().equals(changeVo.getStatus())) {
            if (UserContext.get().getUserUuid(true).equals(changeVo.getOwner().substring(GroupSearch.USER.getValuePlugin().length()))) {
                return true;
            }
        }
        return false;
    }

    /**
     * 
     * @Time:2020年8月4日
     * @Description: 判断当前用是不是处理人
     * @param worker
     * @return boolean
     */
    private boolean checkCurrentUserIsWorker(String worker) {
        if (StringUtils.isNotBlank(worker)) {
            if (worker.startsWith(GroupSearch.USER.getValuePlugin())) {
                String[] split = worker.split("#");
                if (UserContext.get().getUserUuid(true).equals(split[1])) {
                    return true;
                }
            } else if (worker.startsWith(GroupSearch.TEAM.getValuePlugin())) {
                String[] split = worker.split("#");
                List<String> teamUuidList = teamMapper.getTeamUuidListByUserUuid(UserContext.get().getUserUuid(true));
                if (teamUuidList.contains(split[1])) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void activeChangeStep(Long changeId) {
        List<ChangeStepVo> changeStepList = changeMapper.getChangeStepListByChangeId(changeId);
        Map<String, Long> codeChangeStepIdMap = new HashMap<>(changeStepList.size());
        List<String> unActiveStepCodeList = new ArrayList<>();
        List<String> activatedStepCodeList = new ArrayList<>();
        for (ChangeStepVo changeStepVo : changeStepList) {
            if (Objects.equals(changeStepVo.getIsActive(), 0)) {
                unActiveStepCodeList.add(changeStepVo.getCode());
                codeChangeStepIdMap.put(changeStepVo.getCode(), changeStepVo.getId());
            } else if (Objects.equals(changeStepVo.getIsActive(), 1)) {
                if (ChangeStatus.PENDING.getValue().equals(changeStepVo.getStatus())
                    || ChangeStatus.RUNNING.getValue().equals(changeStepVo.getStatus())) {
                    activatedStepCodeList.add(changeStepVo.getCode());
                }
            }
        }
        ChangeStepVo changeStep = new ChangeStepVo();
        unActiveStepCodeList.sort((e1, e2) -> e1.compareToIgnoreCase(e2));
        for (String unActiveCode : unActiveStepCodeList) {
            boolean can = true;
            for (String activatedCode : activatedStepCodeList) {
                if (!compare(unActiveCode, activatedCode)) {
                    can = false;
                    break;
                }
            }
            if (can) {
                /** 激活变更步骤 **/
                Long id = codeChangeStepIdMap.get(unActiveCode);
                changeStep.setId(id);
                changeStep.setIsActive(1);
                changeMapper.updateChangeStepIsActive(changeStep);
                activatedStepCodeList.add(unActiveCode);
            }
        }
    }

    /**
     * 
     * @Time:2020年8月4日
     * @Description: TODO
     * @param unActiveCode
     *            判断是否可以激活的变更步骤编码
     * @param runningCode
     *            待处理或处理中的变更步骤编码
     * @return boolean 返回false，说明unActiveCode步骤需要等待runningCode步骤完成后才能激活
     */
    private boolean compare(String unActiveCode, String runningCode) {
        if (unActiveCode.equals(runningCode)) {
            return true;
        }
        int min = unActiveCode.length() > runningCode.length() ? runningCode.length() : unActiveCode.length();
        for (int i = 0; i < min; i++) {
            char c1 = unActiveCode.charAt(i);
            char c2 = runningCode.charAt(i);
            if (c1 != c2) {
                // 数字
                if (Character.isDigit(c1) && Character.isDigit(c2)) {
                    if (c1 > c2) {
                        // 相同位置的数字，如果未激活步骤大于处理中步骤，说明未激活步骤需要等待处理中步骤完成才能激活
                        return false;
                    }
                } else {
                    // 字母不相等时，说明两个步骤是并行关系，后面的数字不需要比较了
                    return true;
                }
            }
        }
        return true;
    }

    @Override
    public ChangeVo getChangeById(Long changeId) {
        ChangeVo changeVo = changeMapper.getChangeById(changeId);
        if (changeVo == null) {
            throw new ChangeNotFoundException(changeId);
        }
        /** 变更经理 **/
        String owner = changeMapper.getChangeUserByChangeId(changeId);
        if (StringUtils.isNotBlank(owner)) {
            changeVo.setOwner(GroupSearch.USER.getValuePlugin() + owner);
            UserVo userVo = userMapper.getUserBaseInfoByUuid(owner);
            if(userVo != null) {
                UserVo vo = new UserVo();
                BeanUtils.copyProperties(userVo,vo);
                changeVo.setOwnerVo(vo);
//                changeVo.setOwnerName(userVo.getUserName());
//                changeVo.setAvatar(userVo.getAvatar());
//                changeVo.setVipLevel(userVo.getVipLevel());
            }
        }
        /** 变更描述 **/
        String contentHash = changeMapper.getChangeDescriptionContentHashByChangeId(changeVo.getId());
        if (StringUtils.isNotBlank(contentHash)) {
            changeVo.setContent(changeMapper.getChangeContentByHash(contentHash));
        }
        /** 变更附件 **/
        List<Long> fileIdList = changeMapper.getChangeFileIdListByChangeId(changeVo.getId());
        if (CollectionUtils.isNotEmpty(fileIdList)) {
            List<FileVo> fileList = fileMapper.getFileListByIdList(fileIdList);
            if (CollectionUtils.isNotEmpty(fileList)) {
                changeVo.setFileList(fileList);
                changeVo.setFileIdList(fileList.stream().map(FileVo::getId).collect(Collectors.toList()));
            }
        }
        /** 变更模板id **/
        changeVo.setChangeTemplateId(changeMapper.getChangeTemplateIdByChangeId(changeId));
        /** 变更步骤列表 **/
        List<ChangeStepVo> changeStepList = changeMapper.getChangeStepListByChangeId(changeId);
        if (CollectionUtils.isNotEmpty(changeStepList)) {
            /** 遍历变更步骤列表，填充描述内容、附件列表、处理人或组、回复列表 **/
            for (ChangeStepVo changeStepVo : changeStepList) {
                changeStepVo.setChangeStatus(changeVo.getStatus());
                getChangeStepDetail(changeStepVo);
            }
            changeVo.setChangeStepList(changeStepList);
        }
        return changeVo;
    }

    @Override
    public ChangeStepVo getChangeStepDetail(ChangeStepVo changeStepVo) {
        /** 描述内容 **/
        String changeStepContentHash = changeMapper.getChangeStepContentHashByChangeStepId(changeStepVo.getId());
        if (StringUtils.isNotBlank(changeStepContentHash)) {
            changeStepVo.setContent(changeMapper.getChangeContentByHash(changeStepContentHash));
        }

        /** 附件列表 **/
        List<Long> fileIdList = changeMapper.getChangeStepFileIdListByChangeStepId(changeStepVo.getId());
        if (CollectionUtils.isNotEmpty(fileIdList)) {
            List<FileVo> fileList = fileMapper.getFileListByIdList(fileIdList);
            if (CollectionUtils.isNotEmpty(fileList)) {
                changeStepVo.setFileList(fileList);
                changeStepVo.setFileIdList(fileList.stream().map(FileVo::getId).collect(Collectors.toList()));
            }
        }
        /** 处理人或组 **/
        ChangeStepUserVo changeStepUserVo = changeMapper.getChangeStepUserByChangeStepId(changeStepVo.getId());
        if (changeStepUserVo != null && StringUtils.isNotBlank(changeStepUserVo.getUserVo().getUuid())) {
            changeStepVo.setWorkerVo(changeStepUserVo.getUserVo());
            changeStepVo.setWorker(GroupSearch.USER.getValuePlugin() + changeStepUserVo.getUserVo().getUuid());
//            changeStepVo.setWorkerName(changeStepUserVo.getUserName());
//            changeStepVo.setWorkerAvatar(changeStepUserVo.getAvatar());
//            changeStepVo.setWorkerVipLevel(changeStepUserVo.getVipLevel());
        } else {
            ChangeStepTeamVo changeStepTeamVo = changeMapper.getChangeStepTeamByChangeStepId(changeStepVo.getId());
            if (changeStepTeamVo != null && StringUtils.isNotBlank(changeStepTeamVo.getTeamUuid())) {
                TeamVo teamVo = new TeamVo();
                teamVo.setUuid(changeStepTeamVo.getTeamUuid());
                teamVo.setName(changeStepTeamVo.getTeamName());
                changeStepVo.setWorkerVo(teamVo);
                changeStepVo.setWorker(GroupSearch.TEAM.getValuePlugin() + changeStepTeamVo.getTeamUuid());
//                changeStepVo.setWorkerName(changeStepTeamVo.getTeamName());
            }
        }
        /** 回复列表 **/
        changeStepVo.setCommentList(getChangeStepCommentListByChangeStepId(changeStepVo.getId()));
        return changeStepVo;
    }

    @Override
    public boolean isStartableChange(ChangeVo changeVo) {
        if (processTaskIsRunning(changeVo.getId())
            && UserContext.get().getUserUuid(true).equals(changeVo.getOwner().substring(5))) {
            if (ChangeStatus.PENDING.getValue().equals(changeVo.getStatus())) {
                try {
                    if(new Date().after(new SimpleDateFormat(TimeUtil.YYYY_MM_DD_HH_MM).parse(changeVo.getPlanStartTime()))) {
                        return true;
                    }
                }catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public boolean isCompletableChange(ChangeVo changeVo, String userUuid) {
        if (processTaskIsRunning(changeVo.getId())
            && userUuid.equals(changeVo.getOwner().substring(5))) {
            if (ChangeStatus.PAUSED.getValue().equals(changeVo.getStatus())
                || ChangeStatus.SUCCEED.getValue().equals(changeVo.getStatus())
                || ChangeStatus.FAILED.getValue().equals(changeVo.getStatus())) {
                return true;
            } else if (ChangeStatus.RUNNING.getValue().equals(changeVo.getStatus())) {
                for (ChangeStepVo changeStepVo : changeVo.getChangeStepList()) {
                    if (Objects.equals(changeStepVo.getIsActive(), 0)
                        || ChangeStatus.PENDING.getValue().equals(changeStepVo.getStatus())
                        || ChangeStatus.RUNNING.getValue().equals(changeStepVo.getStatus())) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isRecoverableChange(ChangeVo changeVo) {
        if (processTaskIsRunning(changeVo.getId())
            && UserContext.get().getUserUuid(true).equals(changeVo.getOwner().substring(5))
            && CollectionUtils.isNotEmpty(changeVo.getChangeStepList())) {
            if (ChangeStatus.PAUSED.getValue().equals(changeVo.getStatus())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isPauseableChange(ChangeVo changeVo) {
        if (processTaskIsRunning(changeVo.getId())
            && UserContext.get().getUserUuid(true).equals(changeVo.getOwner().substring(5))
            && CollectionUtils.isNotEmpty(changeVo.getChangeStepList())) {
            if (ChangeStatus.RUNNING.getValue().equals(changeVo.getStatus())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isRestartableChange(ChangeVo changeVo) {
        if (processTaskIsRunning(changeVo.getId())
            && UserContext.get().getUserUuid(true).equals(changeVo.getOwner().substring(5))
            && CollectionUtils.isNotEmpty(changeVo.getChangeStepList())) {
            if (!ChangeStatus.PENDING.getValue().equals(changeVo.getStatus())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<ChangeStepCommentVo> getChangeStepCommentListByChangeStepId(Long changeStepId) {
        List<ChangeStepCommentVo> changeStepCommentList =
            changeMapper.getChangeStepCommentListByChangeStepId(changeStepId);
        for (ChangeStepCommentVo commentVo : changeStepCommentList) {
            if (StringUtils.isNotBlank(commentVo.getContentHash())) {
                String content = changeMapper.getChangeContentByHash(commentVo.getContentHash());
                commentVo.setContent(content);
            }
            if (StringUtils.isNotBlank(commentVo.getFileIdListHash())) {
                String fileIdListStr = changeMapper.getChangeContentByHash(commentVo.getFileIdListHash());
                if (StringUtils.isNotBlank(fileIdListStr)) {
                    List<Long> commentFileIdList = JSON.parseArray(fileIdListStr, Long.class);
                    for (Long fileId : commentFileIdList) {
                        FileVo fileVo = fileMapper.getFileById(fileId);
                        if (fileVo != null) {
                            commentVo.getFileList().add(fileVo);
                            commentVo.getFileIdList().add(fileId);
                        }
                    }
                }
            }
            commentVo.setIsEditable(isEditableComment(commentVo));
            commentVo.setIsDeletable(isDeletableComment(commentVo));
        }
        return changeStepCommentList;
    }

    @Override
    public boolean isEditableChange(ChangeVo changeVo) {
        if (processTaskIsRunning(changeVo.getId())
            && UserContext.get().getUserUuid(true).equals(changeVo.getOwner().substring(5))) {
            if (ChangeStatus.PENDING.getValue().equals(changeVo.getStatus())
//                || ChangeStatus.PAUSED.getValue().equals(changeVo.getStatus())
                ) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void startChangeById(Long id, String source) {
        /** 将变更状态设置为“处理中” **/
        ChangeVo changeVo = new ChangeVo();
        changeVo.setId(id);
        changeVo.setStatus(ChangeStatus.RUNNING.getValue());
        changeMapper.updateChangeStatus(changeVo);

        /** 开始变更时，触发激活变更步骤 **/
        activeChangeStep(id);

        /** 开始变更时，触发更新processtask_step_worker和processtask_step_user数据 **/
        ProcessTaskStepChangeVo processTaskStepChangeVo = changeMapper.getProcessTaskStepChangeHandleByChangeId(id);
        if (processTaskStepChangeVo == null) {
            throw new ChangeHandleHasNotStartedException();
        }
        IProcessStepInternalHandler processStepUtilHandler =
                ProcessStepInternalHandlerFactory.getHandler(ChangeProcessStepHandlerType.CHANGEHANDLE.getHandler());
        if (processStepUtilHandler == null) {
            throw new ProcessStepHandlerNotFoundException(ChangeProcessStepHandlerType.CHANGEHANDLE.getHandler());
        }
        processStepUtilHandler.updateProcessTaskStepUserAndWorker(processTaskStepChangeVo.getProcessTaskId(),
                processTaskStepChangeVo.getProcessTaskStepId());

        ProcessTaskStepVo currentProcessTaskStepVo = new ProcessTaskStepVo();
        currentProcessTaskStepVo.setProcessTaskId(processTaskStepChangeVo.getProcessTaskId());
        currentProcessTaskStepVo.setId(processTaskStepChangeVo.getProcessTaskStepId());
        IProcessStepHandlerUtil.audit(currentProcessTaskStepVo, ChangeAuditType.STARTCHANGE);
        IProcessStepHandlerUtil.notify(currentProcessTaskStepVo, ChangeTriggerType.STARTCHANGE);
        IProcessStepHandlerUtil.action(currentProcessTaskStepVo, ChangeTriggerType.STARTCHANGE);

        changeMapper.deleteChangeAutoStartByChangeId(id);
    }

    @Override
    public boolean processTaskIsRunning(Long changeId) {
//        ProcessTaskStepChangeVo processTaskStepChangeVo = changeMapper.getProcessTaskStepChangeHandleByChangeId(changeId);
        Long processTaskId = changeMapper.getProcessTaskIdByChangeId(changeId);
        if (processTaskId != null) {
            ProcessTaskVo processTaskVo = processTaskMapper.getProcessTaskById(processTaskId);
            if (processTaskVo != null && processTaskVo.getIsShow() == 1) {
                return ProcessTaskStatus.RUNNING.getValue().equals(processTaskVo.getStatus());
            }
        }
        return false;
    }

    /**
     * 判断当前用户对变更步骤回复是否有“编辑”操作权限
     * @param changeStepCommentVo 回复对象
     * @return
     */
    @Override
    public int isEditableComment(ChangeStepCommentVo changeStepCommentVo) {
        if (processTaskIsRunning(changeStepCommentVo.getChangeId())) {
            String currentUser = UserContext.get().getUserUuid();
            if(currentUser != null && currentUser.equals(changeStepCommentVo.getFcu())) {
                return 1;
            }
        }
        return 0;
    }

    /**
     * 判断当前用户对变更步骤回复是否有“删除”操作权限
     * @param changeStepCommentVo 回复对象
     * @return
     */
    @Override
    public int isDeletableComment(ChangeStepCommentVo changeStepCommentVo) {
        if (processTaskIsRunning(changeStepCommentVo.getChangeId())) {
            String currentUser = UserContext.get().getUserUuid();
            if(currentUser != null && currentUser.equals(changeStepCommentVo.getFcu())) {
                return 1;
            }
        }
        return 0;
    }
}

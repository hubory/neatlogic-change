/*
Copyright(c) 2023 NeatLogic Co., Ltd. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. 
 */

package neatlogic.module.change.service;

import java.util.List;

import neatlogic.framework.change.dto.ChangeStepCommentVo;
import neatlogic.framework.change.dto.ChangeStepVo;
import neatlogic.framework.change.dto.ChangeVo;

/**
 * @Author:linbq
 * @Time:2020年8月17日
 * @ClassName: ChangeService
 */
public interface ChangeService {

    /**
     * 判断当前用户对变更步骤是否有“开始”操作权限
     *
     * @param changeVo     变更对象
     * @param changeStepVo 变更步骤对象
     * @return
     */
    public boolean isStartableChangeStep(ChangeVo changeVo, ChangeStepVo changeStepVo);

    /**
     * 判断当前用户对变更步骤是否有“取消”操作权限
     *
     * @param changeVo     变更对象
     * @param changeStepVo 变更步骤对象
     * @return
     */
    public boolean isAbortableChangeStep(ChangeVo changeVo, ChangeStepVo changeStepVo);

    /**
     * 判断当前用户对变更步骤是否有“完成”操作权限
     *
     * @param changeVo     变更对象
     * @param changeStepVo 变更步骤对象
     * @return
     */
    public boolean isCompletableChangeStep(ChangeVo changeVo, ChangeStepVo changeStepVo);

    /**
     * 判断当前用户对变更步骤是否有“回复”操作权限
     *
     * @param changeVo     变更对象
     * @param changeStepVo 变更步骤对象
     * @return
     */
    public boolean isCommentableChangeStep(ChangeVo changeVo, ChangeStepVo changeStepVo);

    /**
     * 判断当前用户对变更步骤是否有“编辑”操作权限
     *
     * @param changeVo     变更对象
     * @param changeStepVo 变更步骤对象
     * @return
     */
    public boolean isEditableChangeStep(ChangeVo changeVo, ChangeStepVo changeStepVo);

    /**
     * 判断当前用户对变更是否有“批量更新处理人”操作权限
     *
     * @param changeVo 变更对象
     * @return
     */
    public boolean isBatchUpdateChangeStepWorker(ChangeVo changeVo);

    /**
     * 完成某个变更步骤时，调用该方法激活其他可激活的变更步骤
     *
     * @param changeId 变更id
     */
    public void activeChangeStep(Long changeId);

    /**
     * 查询变更的详细信息
     *
     * @param id 变更id
     * @return
     */
    public ChangeVo getChangeById(Long id);

    /**
     * 查询变更步骤的详细信息
     *
     * @param changeStepVo 变更步骤对象
     * @return
     */
    public ChangeStepVo getChangeStepDetail(ChangeStepVo changeStepVo);

    /**
     * 判断当前用户对变更是否有“开始”操作权限
     *
     * @param changeVo 变更对象
     * @return
     */
    public boolean isStartableChange(ChangeVo changeVo);

    /**
     * 判断当前用户对变更是否有“完成”操作权限
     *
     * @param changeVo 变更对象
     * @param userUuid 用户uuid
     * @return
     */
    public boolean isCompletableChange(ChangeVo changeVo, String userUuid);

    /**
     * 判断当前用户对变更是否有“恢复”操作权限
     *
     * @param changeVo 变更对象
     * @return
     */
    public boolean isRecoverableChange(ChangeVo changeVo);

    /**
     * 判断当前用户对变更是否有“暂停”操作权限
     *
     * @param changeVo 变更对象
     * @return
     */
    public boolean isPauseableChange(ChangeVo changeVo);

    /**
     * 判断当前用户对变更是否有“重新开始”操作权限
     *
     * @param changeVo 变更对象
     * @return
     */
    public boolean isRestartableChange(ChangeVo changeVo);

    /**
     * 根据变更步骤id查询回复列表
     *
     * @param changeStepId 变更步骤id
     * @return
     */
    public List<ChangeStepCommentVo> getChangeStepCommentListByChangeStepId(Long changeStepId);

    /**
     * 判断当前用户对变更是否有“编辑”操作权限
     *
     * @param changeVo 变更对象
     * @return
     */
    public boolean isEditableChange(ChangeVo changeVo);

    /**
     * @param id 变更id
     * @param source 来源
     */
    public void startChangeById(Long id, String source);

    /**
     * 变更对应的工单及工单步骤是否在进行中
     *
     * @param changeId 变更id
     * @return
     */
    public boolean processTaskIsRunning(Long changeId);

    /**
     * 判断当前用户对变更步骤回复是否有“编辑”操作权限
     *
     * @param changeStepCommentVo 回复对象
     * @return
     */
    public int isEditableComment(ChangeStepCommentVo changeStepCommentVo);

    /**
     * 判断当前用户对变更步骤回复是否有“删除”操作权限
     *
     * @param changeStepCommentVo 回复对象
     * @return
     */
    public int isDeletableComment(ChangeStepCommentVo changeStepCommentVo);
}

package neatlogic.module.change.service;

import java.util.List;

import neatlogic.framework.dto.TeamVo;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import neatlogic.framework.common.constvalue.GroupSearch;
import neatlogic.framework.file.dao.mapper.FileMapper;
import neatlogic.module.change.dao.mapper.ChangeMapper;
import neatlogic.module.change.dao.mapper.ChangeTemplateMapper;
import neatlogic.framework.change.dto.ChangeSopStepTeamVo;
import neatlogic.framework.change.dto.ChangeSopStepUserVo;
import neatlogic.framework.change.dto.ChangeSopStepVo;
import neatlogic.framework.change.dto.ChangeSopVo;
import neatlogic.framework.change.dto.ChangeTemplateVo;
import neatlogic.framework.change.exception.ChangeSopNotFoundException;
import neatlogic.framework.change.exception.ChangeTemplateNotFoundEditTargetException;

@Service
public class ChangeTemplateServiceImpl implements ChangeTemplateService{
    
    @Autowired
    private ChangeTemplateMapper changeTemplateMapper;
    
    @Autowired
    private ChangeMapper changeMapper;

    @Autowired
    private FileMapper fileMapper;

    @Override
    public ChangeSopVo getChangeSopById(Long changeSopId) {
        ChangeSopVo changeSopVo = changeTemplateMapper.getChangeSopById(changeSopId);
        if (changeSopVo == null) {
            throw new ChangeSopNotFoundException(changeSopId);
        }
        /** 变更步骤列表 **/
        List<ChangeSopStepVo> changeSopStepList = changeTemplateMapper.getChangeSopStepListByChangeSopId(changeSopId);
        if (CollectionUtils.isNotEmpty(changeSopStepList)) {
            changeSopStepList.sort((e1, e2) -> e1.getCode().compareTo(e2.getCode()));
            /** 遍历变更步骤列表，填充描述内容、附件列表、处理人或组 **/
            for (ChangeSopStepVo changeSopStepVo : changeSopStepList) {
                getChangeSopStepDetail(changeSopStepVo);
            }
            changeSopVo.setChangeSopStepList(changeSopStepList);
        }
        return changeSopVo;
    }
    @Override
    public ChangeSopStepVo getChangeSopStepDetail(ChangeSopStepVo changeSopStepVo) {
        /** 描述内容 **/
        String changeStepContentHash = changeTemplateMapper.getChangeSopStepContentHashByChangeSopStepUuid(changeSopStepVo.getUuid());
        if (StringUtils.isNotBlank(changeStepContentHash)) {
            changeSopStepVo.setContent(changeMapper.getChangeContentByHash(changeStepContentHash));
        }

        /** 附件列表 **/
        List<Long> fileIdList = changeTemplateMapper.getChangeSopStepFileIdListByChangeSopStepUuid(changeSopStepVo.getUuid());
        if (CollectionUtils.isNotEmpty(fileIdList)) {
            changeSopStepVo.setFileList(fileMapper.getFileListByIdList(fileIdList));
            changeSopStepVo.setFileIdList(fileIdList);
        }
        /** 处理人或组 **/
        ChangeSopStepUserVo changeSopStepUserVo = changeTemplateMapper.getChangeSopStepUserByChangeSopStepUuid(changeSopStepVo.getUuid());
        if (changeSopStepUserVo != null && StringUtils.isNotBlank(changeSopStepUserVo.getUserVo().getUuid())) {
            changeSopStepVo.setWorkerVo(changeSopStepUserVo.getUserVo());
            changeSopStepVo.setWorker(GroupSearch.USER.getValuePlugin() + changeSopStepUserVo.getUserVo().getUuid());
//            changeSopStepVo.setWorkerName(changeSopStepUserVo.getUserName());
//            changeSopStepVo.setWorkerAvatar(changeSopStepUserVo.getAvatar());
//            changeSopStepVo.setWorkerVipLevel(changeSopStepUserVo.getVipLevel());
        } else {
            ChangeSopStepTeamVo changeSopStepTeamVo = changeTemplateMapper.getChangeSopStepTeamByChangeSopStepUuid(changeSopStepVo.getUuid());
            if (changeSopStepTeamVo != null && StringUtils.isNotBlank(changeSopStepTeamVo.getTeamUuid())) {
                TeamVo teamVo = new TeamVo();
                teamVo.setUuid(changeSopStepTeamVo.getTeamUuid());
                teamVo.setName(changeSopStepTeamVo.getTeamName());
                changeSopStepVo.setWorkerVo(teamVo);
                changeSopStepVo.setWorker(GroupSearch.TEAM.getValuePlugin() + changeSopStepTeamVo.getTeamUuid());
//                changeSopStepVo.setWorkerName(changeSopStepTeamVo.getTeamName());
            }
        }
        return changeSopStepVo;
        
    }

    @Override
    public ChangeTemplateVo getChangeTemplateById(Long changeTemplateId) {
        ChangeTemplateVo changeTemplateVo = changeTemplateMapper.getChangeTemplateById(changeTemplateId);
        if(changeTemplateVo == null) {
            throw new ChangeTemplateNotFoundEditTargetException(changeTemplateId);
        }
        List<Long> changeSopIdList = changeTemplateMapper.getChangeSopIdListByChangeTemplateId(changeTemplateId);
        if(CollectionUtils.isNotEmpty(changeSopIdList)) {
            changeTemplateVo.setChangeSopIdList(changeSopIdList);
            for(Long changeSopId : changeSopIdList) {
                changeTemplateVo.getChangeSopList().add(getChangeSopById(changeSopId));
            }
        }
        return changeTemplateVo;
    }

}

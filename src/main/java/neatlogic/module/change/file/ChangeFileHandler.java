/*
 * Copyright(c) 2023 NeatLogic Co., Ltd. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package neatlogic.module.change.file;

import neatlogic.framework.auth.core.AuthActionChecker;
import neatlogic.module.change.auth.label.CHANGE_MODIFY;
import neatlogic.framework.crossover.CrossoverServiceFactory;
import neatlogic.framework.exception.type.PermissionDeniedException;
import neatlogic.framework.file.core.FileTypeHandlerBase;
import neatlogic.framework.file.dto.FileVo;
import neatlogic.framework.process.crossover.ICatalogCrossoverService;
import neatlogic.framework.process.crossover.IProcessTaskCrossoverService;
import neatlogic.framework.process.dao.mapper.ProcessTaskMapper;
import neatlogic.framework.process.dto.ProcessTaskVo;
import neatlogic.framework.process.exception.processtask.ProcessTaskNotFoundException;
import neatlogic.module.change.dao.mapper.ChangeMapper;
import neatlogic.module.change.dao.mapper.ChangeTemplateMapper;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ChangeFileHandler extends FileTypeHandlerBase {

    @Resource
    private ChangeMapper changeMapper;

    @Resource
    private ChangeTemplateMapper changeTemplateMapper;

    @Resource
    ProcessTaskMapper processTaskMapper;

    @Override
    public boolean valid(String userUuid, FileVo fileVo, JSONObject jsonObj) throws PermissionDeniedException {
        Long fileId = fileVo.getId();
        List<ProcessTaskVo> processTaskVoList;
        /**
         * change的fileId会存在change_step_file、change_file、 processtask_file、change_sop_step_file 四张表里面
         * 因此分两步：
         * 1、首先校验前三张表，校验该用户是否工单干系人或拥有该工单服务的上报权限，满足才允许下载
         * 2、若第一步不成立，则确认fileId是否在第四张表里面，否则没有下载权限
         *
         */

        //从前三张表里取出对应的processTaskVoList
        List<Long> changeIdList = changeMapper.getChangeIdListByFileId(fileId);
        if (CollectionUtils.isNotEmpty(changeIdList)) {
            processTaskVoList = changeMapper.getProcessTaskVoListByChangeIdList(changeIdList);
        } else {
            processTaskVoList = processTaskMapper.getProcessTaskStepVoListByFileId(fileId);
        }

        if (CollectionUtils.isNotEmpty(processTaskVoList)) {
            //1
            ICatalogCrossoverService iCatalogCrossoverService = CrossoverServiceFactory.getApi(ICatalogCrossoverService.class);
            if (iCatalogCrossoverService.channelIsAuthority(processTaskVoList.get(0).getChannelUuid(), userUuid)) {
                return true;
            }
            IProcessTaskCrossoverService processTaskCrossoverService = CrossoverServiceFactory.getApi(IProcessTaskCrossoverService.class);
            return processTaskCrossoverService.getProcessFileHasDownloadAuthWithFileIdAndProcessTaskIdList(fileId, processTaskVoList.stream().map(ProcessTaskVo::getId).collect(Collectors.toList()));
        } else {
            //2
            if (changeTemplateMapper.checkChangeSopStepFileIdIsExists(fileId) > 0) {
                if (!AuthActionChecker.check(CHANGE_MODIFY.class)) {
                    throw new PermissionDeniedException(CHANGE_MODIFY.class);
                }
            } else {
                throw new ProcessTaskNotFoundException();
            }
        }
        throw new ProcessTaskNotFoundException();
    }

    @Override
    public String getDisplayName() {
        return "变更附件";
    }


    @Override
    public String getName() {
        return "CHANGE";
    }

    @Override
    protected boolean myDeleteFile(FileVo fileVo, JSONObject paramObj) {
        return true;
    }
}

package neatlogic.module.change.operationauth.handler;

import neatlogic.framework.process.operationauth.core.IOperationAuthHandlerType;
import neatlogic.framework.util.$;

public enum ChangeOperationAuthHandlerType implements IOperationAuthHandlerType {
    CHANGECREATE("changecreate", "变更创建组件"),
    CHANGEHANDLE("changehandle", "变更处理组件");

    private ChangeOperationAuthHandlerType(String value, String text) {
        this.value = value;
        this.text = text;
    }
    private String value;
    private String text;
    @Override
    public String getText() {
        return $.t(text);
    }
    @Override
    public String getValue() {
        return value;
    }
}

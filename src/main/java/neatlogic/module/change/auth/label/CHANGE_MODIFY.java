package neatlogic.module.change.auth.label;

import neatlogic.framework.auth.core.AuthBase;
import neatlogic.framework.process.auth.PROCESS_BASE;

import java.util.Collections;
import java.util.List;

public class CHANGE_MODIFY extends AuthBase {

    @Override
    public String getAuthDisplayName() {
        return "变更管理权限";
    }

    @Override
    public String getAuthIntroduction() {
        return "对变更进行添加、修改和删除";
    }

    @Override
    public String getAuthGroup() {
        return "process";
    }

    @Override
    public Integer getSort() {
        return 2;
    }

    @Override
    public List<Class<? extends AuthBase>> getIncludeAuths(){
        return Collections.singletonList(PROCESS_BASE.class);
    }
}
